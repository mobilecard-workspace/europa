package crypto;

import org.apache.log4j.Logger;
import org.bouncycastle.crypto.BlockCipher;
import org.bouncycastle.crypto.BufferedBlockCipher;
import org.bouncycastle.crypto.engines.DESedeEngine;
import org.bouncycastle.crypto.modes.CBCBlockCipher;
import org.bouncycastle.crypto.paddings.PaddedBufferedBlockCipher;
import org.bouncycastle.crypto.params.KeyParameter;

import com.addcel.utils.AddcelCrypto;

import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

public class TripleDes {
	private static Logger log = Logger.getLogger(AddcelCrypto.class);
	
	private BlockCipher engine = new DESedeEngine();

	public String encrypt(String key, String plainText) {
		String resp = null;
		try {
			byte[] pbtKey = toBytes(key);
//			byte[] pbtKey = FunctionByte.hex2byte(key);
			byte[] ptBytes = plainText.getBytes();
			log.debug("KEY Length: " + pbtKey.length);
			log.debug("CAD Length: " + ptBytes.length);
			
			BufferedBlockCipher cipher = new PaddedBufferedBlockCipher(new CBCBlockCipher(engine));
			cipher.init(true, new KeyParameter(pbtKey));
			byte[] rv = new byte[cipher.getOutputSize(ptBytes.length)];
			int tam = cipher.processBytes(ptBytes, 0, ptBytes.length, rv, 0);
			
			cipher.doFinal(rv, tam);
			resp = new BASE64Encoder().encode(rv);
			
			log.debug("resp Length: " + resp.getBytes().length);
		} catch (Exception ce) {
			ce.printStackTrace();
		}
		return resp;
	}

	public String decrypt(String key, String plainText) {
		String resp = null;
		try {
			byte[] pbtKey = toBytes(key);
//			byte[] pbtKey = FunctionByte.hex2byte(key);
			byte[] ptBytes = new BASE64Decoder().decodeBuffer(plainText);
			
			log.debug("KEY Length: " + pbtKey.length);
			log.debug("CAD Length: " + ptBytes.length);
			
			BufferedBlockCipher cipher = new PaddedBufferedBlockCipher(	new CBCBlockCipher(engine));
			cipher.init(false, new KeyParameter(pbtKey));
			byte[] rv = new byte[cipher.getOutputSize(ptBytes.length)];
			int tam = cipher.processBytes(ptBytes, 0, ptBytes.length, rv, 0);
			
			cipher.doFinal(rv, tam);
			resp = new String(rv);
		} catch (Exception ce) {
			ce.printStackTrace();
		}
		return resp;
	}
	
	/**
     * Metodo para convertir un String en un arreglo de 8 bytes
     * @param s String a convertir
     * @return Un arreglo de 8 bytes
     */
    private static byte[] toBytes(String s){
        byte Resultado[] = null;
        try {
            Resultado = new byte[s.length() / 2];
            for (int i = 0; i < Resultado.length; i++){
                Resultado[i] = (byte)Integer.parseInt(s.substring(i * 2, i * 2 + 2), 16);
            }
            return Resultado;
        }catch(Exception e){
            return Resultado;
        }
    }
}

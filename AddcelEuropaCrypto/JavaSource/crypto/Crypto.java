package crypto;

import org.apache.log4j.Logger;

public class Crypto {
	private static Logger log = Logger.getLogger(Crypto.class);
	
	public static String aesEncrypt(String seed, String cleartext) {
		String encryptedText = null;
		try{
			return AESBinFormat.encode(replaceConAcento(cleartext), seed);
		}catch(Exception e){
			encryptedText = "";
			log.error("Error aesEncrypt. AESBinFormat.encode ",e);
		}
		
		return encryptedText;
	}
	
	public static String aesDecrypt(String seed, String encrypted) {
		String decryptedText;
		
		try{
			return replaceHTMLAcento(AESBinFormat.decode(encrypted, seed));
		}catch(Exception e){
			decryptedText = "";
			log.error("Error aesDecrypt. replaceHTMLAcento: ",e);
		}
		
		return decryptedText;
	}
        
    public static String sha1(String s) {
            if (s != null){
                try {
                    SHA1Digest digest = new SHA1Digest();
                    byte[] bb = s.getBytes();
                    digest.update(bb, 0, bb.length);

                    byte[] digestValue = new byte[digest.getDigestSize()];
                    digest.doFinal(digestValue, 0);

                    return MD5.toHex(digestValue);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            return "";
        }
    
         public static String replaceHTMLAcento(String text){
        
//           String Res = text.replace("&Ntilde;","Ñ");
//                  Res = Res.replace ("&ntilde;","ñ");
//                  Res = Res.replace ("&Aacute;","Á");
//                  Res = Res.replace ("&aacute;","á");
//                  Res = Res.replace ("&Eacute;","E");
//                  Res = Res.replace ("&eacute;","é");
//                  Res = Res.replace ("&Iacute;","Í");
//                  Res = Res.replace ("&iacute;","í");
//                  Res = Res.replace ("&Oacute;","Ó");
//                  Res = Res.replace ("&oacute;","ó");
//                  Res = Res.replace ("&Uacute;","Ú");
//                  Res = Res.replace ("&uacute;","ú");
//        return Res.toString();
          return text;
    }
        

    
    public static String replaceConAcento(String text){
//        StringBuffer sBuffer = new StringBuffer();
//        for(int i=0; i<text.length(); i++){
//            if(text.charAt(i)=='Ñ')
//                sBuffer.append("&Ntilde;");
//            else if(text.charAt(i)=='ñ')
//                sBuffer.append("&ntilde;");
//            else if(text.charAt(i)=='Á')
//                sBuffer.append("&Aacute;");
//            else if(text.charAt(i)=='á')
//                sBuffer.append("&aacute;");
//            else if(text.charAt(i)=='É')
//                sBuffer.append("&Eacute;");
//            else if(text.charAt(i)=='é')
//                sBuffer.append("&eacute;");
//            else if(text.charAt(i)=='Í')
//                sBuffer.append("&Iacute;");
//            else if(text.charAt(i)=='í')
//                sBuffer.append("&iacute;");
//            else if(text.charAt(i)=='Ó')
//                sBuffer.append("&Oacute;");
//            else if(text.charAt(i)=='ó')
//                sBuffer.append("&oacute;");
//            else if(text.charAt(i)=='Ú')
//                sBuffer.append("&Uacute;");
//            else if(text.charAt(i)=='ú')
//                sBuffer.append("&uacute;");
//            else
//                sBuffer.append(text.charAt(i));           
//        }
        
//        return sBuffer.toString();
        return text;
    }

        
}
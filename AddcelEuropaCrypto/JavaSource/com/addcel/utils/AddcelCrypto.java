package com.addcel.utils;

import org.apache.log4j.Logger;

import crypto.Crypto;
import crypto.TripleDes;

public class AddcelCrypto {
	private static Logger log = Logger.getLogger(AddcelCrypto.class);
	
	private final static String keyCat = "U5KVpKwB6AJ9kTaC19w46N7ciMUOqhf0";
	private final static String keyTarjeta = "5525963513";

	public static String encryptHard(String json) {
		String respuesta = null;
		try{
			respuesta = encrypt(keyCat, json);
		}catch(Exception e){
			log.error("Ocurrio un error durante el proceso Asf.", e);
			respuesta = null;
		}
		return respuesta;
	}

	public static String encryptSensitive(String key, String json) {
		String respuesta = null;
		try{
			json = encrypt(parsePass(key).toString(), json);
			respuesta = mergeAsf(json, key);
			//log.debug(new StringBuffer("CADENA mergeAsf respuesta: ").append( respuesta));
		}catch(Exception e){
			log.error("Ocurrio un error durante el proceso decryptSensitive.", e);
			respuesta = null;
		}finally{
			key = null;
		}
		return respuesta;

	}
	
	public static String encryptTarjeta(String json) {
		String respuesta = null;
		try{
			//log.info(new StringBuffer("CADENA : ").append( json));
			respuesta = encrypt(parsePass(keyTarjeta).toString(), json);
			//log.info(new StringBuffer("CADENA respuesta: ").append( respuesta));
		}catch(Exception e){
			log.error("Ocurrio un error durante el proceso decryptSensitive.", e);
			respuesta = null;
		}
		return respuesta;

	}
	
	public static String encryptPassword(String json) {
		String respuesta = null;
		try{
			//log.info(new StringBuffer("CADENA : ").append( json));
			respuesta = encrypt(parsePass(json).toString(), json);
			//log.info(new StringBuffer("CADENA respuesta: ").append( respuesta));
		}catch(Exception e){
			log.error("Ocurrio un error durante el proceso encryptPassword: {}", e);
			respuesta = null;
		}
		return respuesta;

	}
	
	public static String encrypt(String key, String json) {
		String respuesta = null;
		try{
			//log.debug(new StringBuffer("CADENA encrypt key: ").append( key ));
			log.debug(new StringBuffer("CADENA encrypt json: ").append( json));
			respuesta = Crypto.aesEncrypt(key, json);
			log.debug(new StringBuffer("CADENA encrypt respuesta: ").append( respuesta));
		}catch(Exception e){
			log.error("Ocurrio un error durante el proceso decrypt.", e);
			respuesta = null;
		}finally{
			key = null;
			json = null;
		}
		return respuesta;
	}
	
	public static String decryptTarjeta(String json) {
		String respuesta = null;
		try{
			//log.info(new StringBuffer("CADENA : ").append( json));
			respuesta = decrypt(parsePass(keyTarjeta).toString(), json);
			//log.info(new StringBuffer("CADENA respuesta: ").append( respuesta));
		}catch(Exception e){
			log.error("Ocurrio un error durante el proceso decryptSensitive.", e);
			respuesta = null;
		}
		return respuesta;
	}
	
	public static String decryptHard(String json) {
		String respuesta = null;
		try{
			respuesta = decrypt(keyCat, json);
		}catch(Exception e){
			log.error("Ocurrio un error durante el proceso Asf.", e);
			respuesta = null;
		}
		return respuesta;
	}

	public static String decryptSensitive(String json) {
		String key = null;
		String [] cad = null;
		String respuesta = null;
		try{
			//log.debug(new StringBuffer("CADENA decrypt original json: ").append( json ));
			cad = Asf(json);
			key = cad[0];
			json = cad[1];
			respuesta = decrypt(key, json);
		}catch(Exception e){
			log.error("Ocurrio un error durante el proceso decryptSensitive.", e);
			respuesta = null;
		}finally{
			key = null;
			cad = null;
		}
		return respuesta;
	}
	
	public static String decrypt(String key, String json) {
		String respuesta = null;
		try{
			//log.debug(new StringBuffer("CADENA decrypt key: ").append( key ));
			log.debug(new StringBuffer("CADENA decrypt json: ").append( json));
			respuesta = Crypto.aesDecrypt(key, json.replace(" ", "+"));
			log.debug(new StringBuffer("CADENA decrypt respuesta: ").append( respuesta));
		}catch(Exception e){
			log.error("Ocurrio un error durante el proceso decrypt.", e);
			respuesta = null;
		}finally{
			key = null;
		}
		return respuesta;
	}
	
	private static String[] Asf(String cadena){
		
		StringBuffer key = new StringBuffer();
		String cadenaCompleta = "";
		String[] res= new String [3];
		int Long = 0;
		String tempo="";
	    try{
	    
			Long = Integer.parseInt(cadena.substring(0, 2));
//			log.debug("Longuitud: " + String.valueOf(Long));
//			log.debug("Longuitud cadena: " + String.valueOf(cadena.length()));
			
			cadenaCompleta = cadena.substring(2);
//			log.debug( cadenaCompleta);
			
		
			if(Long <= 8){
				key.append(cadena.substring(21,23)).append( cadena.substring(25,27)) .
					append(  cadena.substring(29,31)).append(  cadena.substring(33,35));
				cadenaCompleta = cadenaCompleta.substring(0,19) + cadenaCompleta.substring(21) ;
				cadenaCompleta = cadenaCompleta.substring(0,21) + cadenaCompleta.substring(23) ;
				cadenaCompleta = cadenaCompleta.substring(0,23) + cadenaCompleta.substring(25) ;
				cadenaCompleta = cadenaCompleta.substring(0,25) + cadenaCompleta.substring(27) ;
			}
//			else{
//				for (int i =0, j = 21; i < ciclos[Long]; i++, j+=4){
//					key.append(cadena.substring(j,j+2));
//					cadenaCompleta = cadenaCompleta.substring(i,i+19) + cadenaCompleta.substring(i+21) ;
//					log.debug("i: " + i + "   key: " + key + "    cadena: " + cadenaCompleta);
//				}
//			}
//			log.debug(new StringBuffer("Paso 0: ").append( key ));
			
			if(Long == 9){
				key.append( cadena.substring(21,23) ).append( cadena.substring(25,27) ).
					append( cadena.substring(29,31) ).append( cadena.substring(33,35)).
					append( cadena.substring(37,38) );
				cadenaCompleta = cadenaCompleta.substring(0,19)+ cadenaCompleta.substring(21) ;
				 cadenaCompleta = cadenaCompleta.substring(0,21)+ cadenaCompleta.substring(23) ;
				 cadenaCompleta = cadenaCompleta.substring(0,23)+ cadenaCompleta.substring(25) ;
				 cadenaCompleta = cadenaCompleta.substring(0,25)+ cadenaCompleta.substring(27) ;
				 cadenaCompleta = cadenaCompleta.substring(0,27)+ cadenaCompleta.substring(28) ;
			}
//			log.debug(new StringBuffer("Paso 1: ").append( key ));
			
			if(Long == 10){
				key.append( cadena.substring(21,23) ).append( cadena.substring(25,27)).
					append( cadena.substring(29,31)).append( cadena.substring(33,35)).
					append( cadena.substring(37,39) );
				cadenaCompleta = cadenaCompleta.substring(0,19)+ cadenaCompleta.substring(21) ;
				 cadenaCompleta = cadenaCompleta.substring(0,21)+ cadenaCompleta.substring(23) ;
				 cadenaCompleta = cadenaCompleta.substring(0,23)+ cadenaCompleta.substring(25) ;
				 cadenaCompleta = cadenaCompleta.substring(0,25)+ cadenaCompleta.substring(27) ;
				 cadenaCompleta = cadenaCompleta.substring(0,27)+ cadenaCompleta.substring(29) ;
			}
			
//			log.debug(new StringBuffer("Paso 2: ").append( key ));
			if(Long == 11){
				key.append( cadena.substring(21,23) ).append( cadena.substring(25,27) ).
					append( cadena.substring(29,31) ).append( cadena.substring(33,35)).
					append( cadena.substring(37,38) ).append( cadena.substring(38,39)).
					append( cadena.substring(41,42) );
				cadenaCompleta = cadenaCompleta.substring(0,19)+ cadenaCompleta.substring(21) ;
				 cadenaCompleta = cadenaCompleta.substring(0,21)+ cadenaCompleta.substring(23) ;
				 cadenaCompleta = cadenaCompleta.substring(0,23)+ cadenaCompleta.substring(25) ;
				 cadenaCompleta = cadenaCompleta.substring(0,25)+ cadenaCompleta.substring(27) ;
				 cadenaCompleta = cadenaCompleta.substring(0,27)+ cadenaCompleta.substring(29) ;
				 cadenaCompleta = cadenaCompleta.substring(0,29)+ cadenaCompleta.substring(30) ;
			}
//			log.debug(new StringBuffer("Paso 3: ").append( key ));
			
			if(Long == 12){
				key.append( cadena.substring(21,23) ).append( cadena.substring(25,27)) .
					append( cadena.substring(29,31) ).append( cadena.substring(33,35)).
					append( cadena.substring(37,38) ).append( cadena.substring(38,39)) .
					append( cadena.substring(41,42) ).append( cadena.substring(42,43));
				
				cadenaCompleta = cadenaCompleta.substring(0,19)+ cadenaCompleta.substring(21) ;
				 cadenaCompleta = cadenaCompleta.substring(0,21)+ cadenaCompleta.substring(23) ;
				 cadenaCompleta = cadenaCompleta.substring(0,23)+ cadenaCompleta.substring(25) ;
				 cadenaCompleta = cadenaCompleta.substring(0,25)+ cadenaCompleta.substring(27) ;
				 cadenaCompleta = cadenaCompleta.substring(0,27)+ cadenaCompleta.substring(29) ;
				 cadenaCompleta = cadenaCompleta.substring(0,29)+ cadenaCompleta.substring(31) ;
			}
			
			for(int y=Long-1; y != -1; y--){
				tempo += key.substring(y, y+1) ;
			}
	
		    res[2] = key.toString();
			key = parsePass(tempo);
			res[0] = key.toString();
			res[1] = cadenaCompleta.toString();
			
//		    log.debug(new StringBuffer("Key: ").append( key));
//		    log.debug(new StringBuffer("cadenaCompleta: ").append( cadenaCompleta));
		    
	    }catch (Exception e){
	    	log.error("Ocurrio un error durante el proceso Asf.", e);
	    	res = null;
	    }finally{
	    	key = null;
			cadenaCompleta = null;
			tempo = null;
	    }
		return res;
	}
	
	private static StringBuffer parsePass(String pass){
	    int len = pass.length();
	    StringBuffer key = new StringBuffer();
	    
	    for (int i =0; i < 32 /len; i++){
	          key.append( pass );
	    }
	    
	    int carry = 0;
	    while (key.length() < 32){
	          key.append( pass.charAt(carry));
	          carry++;
	    }
	    return key;
	}
	
	private static String mergeAsf(String json, String key){
		StringBuffer result = new StringBuffer();
		StringBuilder builder = null;
		int offset = 0;
		String next = null;
		String sub1 = null;
		String sub2 = null;
		try{
			builder = new StringBuilder(key);
			key = builder.reverse().toString();
	
			result.append( ( Integer.toString(key.length()).length() < 2 ) ? "0" + key.length() : key.length());
	
			sub1 = json.substring(0, 19);
			sub2 = json.substring(19, json.length());
	
			result.append( sub1 );
	
			for (int i =0; i < key.length(); i+=2){
				offset = ( (i+2) <= key.length() ) ? (i+2) : (i+1);
				next = key.substring(i, offset);
	
				result.append( next);
				result.append( sub2.substring(0, 2));
				sub2 = sub2.substring(2);
				
				
			}
			result.append( sub2);
		}catch(Exception e){
			
		}
		return result.toString();
	}
	
	public static String encryptTripleDes(String key, String cadena){
		TripleDes tripleDes = null;
		String respuesta = null;
		try{
			log.debug("Cadena a cifrada 3DES: " + cadena);
			tripleDes = new TripleDes();
			respuesta = tripleDes.encrypt(key, cadena);
			respuesta = respuesta.replaceAll("\r", "").replaceAll("\n", "");
			log.debug("Cadena cifrada 3DES: " + respuesta);
		}catch(Exception e){
			log.error("Ocurrio un error durante el proceso decryptSensitive.", e);
			respuesta = null;
		}finally{
			key = null;
			cadena = null;
		}
		return respuesta;
	}
	
	public static String decryptTripleDes(String key, String cadena){
		TripleDes tripleDes = null;
		String respuesta = null;
		try{
			log.debug("Cadena a descifrada 3DES: " + cadena);
			tripleDes = new TripleDes();
			respuesta = tripleDes.decrypt(key, cadena);
			log.debug("Cadena descifrada 3DES: " + respuesta);
		}catch(Exception e){
			log.error("Ocurrio un error durante el proceso decryptSensitive.", e);
			respuesta = null;
		}finally{
			key = null;
			cadena = null;
		}
		return respuesta;
	}

}

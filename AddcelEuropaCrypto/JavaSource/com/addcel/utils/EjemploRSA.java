package com.addcel.utils;

import java.security.*;
import java.security.spec.*;

import   javax.crypto.*;
import   javax.crypto.interfaces.*;
import   javax.crypto.spec.*;

import java.io.*;

import org.bouncycastle.jce.provider.BouncyCastleProvider;
// Necesario para usar el provider Bouncy Castle (BC)
//    Para compilar incluir el fichero JAR en el classpath
// 
public class EjemploRSA  {
   public static void main (String[] args) throws Exception {	
      
      // Anadir provider JCE (provider por defecto no soporta RSA)
      Security.addProvider(new BouncyCastleProvider());  // Cargar el provider BC
            
      System.out.println("1. Creando claves publica y privada");

      // PASO 1: Crear e inicializar el par de claves RSA DE 512 bits
      KeyPairGenerator keyGen = KeyPairGenerator.getInstance("RSA", "BC"); // Hace uso del provider BC
      keyGen.initialize(2048);  // tamano clave 512 bits
      
      for(int i = 0; i < 50; i ++){
    	  KeyPair clavesRSA = keyGen.generateKeyPair();
          PrivateKey clavePrivada = clavesRSA.getPrivate();
          PublicKey clavePublica = clavesRSA.getPublic();

          System.out.print("[ " + i + " ]  1. Clave privada: " + clavePrivada.toString());
          System.out.println();
          System.out.print("[ " + i + " ]  2. Clave publica: " + clavePublica.toString());
//          mostrarBytes("bcda5927a891ab69e0231c89bbc5350c1ed306ba4612e70c3b7ed02a4997f6d4b713405838a9a01a785a8935603f8186d62c3ccd77981a0925c9d2c8928d0f282f82f6601462eeedd3928e686deb9ad91c268f3fd0b95eb1839441144f48b97ce1f9259bb7ffbee47d5311f3c397a352eef82c8e342b3258f62ea502db5726e2d06591e088eb826a275a8a038df3289cc0372170944f4299a2917872f78b7f4779436c9268c02cd17c50267f5b4faf1f26c8b0c576aa4f32ba2a496ffcc6fbb1a56943e1047f0c45f01c73f3be826c504012488832849419141e0d7e2bb5c3a4fd2a1d30c9876f76fbef7d7f6ce8ad89b9804aead8938b75532c05185e5f0299".getBytes());
          System.out.print("********************************************************************************\n\n\n\n");
      }
      
      
      
//      System.out.print("2. Introducir Texto Plano (max. 64 caracteres): ");
//      byte[] bufferPlano = leerLinea(System.in);
//
//      // PASO 2: Crear cifrador RSA
//      Cipher cifrador = Cipher.getInstance("RSA", "BC"); // Hace uso del provider BC
//      /************************************************************************
//       * IMPORTANTE: En BouncyCastle el algoritmo RSA no funciona realmente en modo ECB
//       *		  * No divide el mensaje de entrada en bloques
//       *                  * Solo cifra los primeros 512 bits (tam. clave)
//       *		  * Para cifrar mensajes mayores, habría que hacer la 
//       *                    división en bloques "a mano"
//       ************************************************************************/
//      
//      
//      // PASO 3a: Poner cifrador en modo CIFRADO 
//      cifrador.init(Cipher.ENCRYPT_MODE, clavePublica);  // Cifra con la clave publica
//
//      System.out.println("3a. Cifrar con clave publica");
//      byte[] bufferCifrado = cifrador.doFinal(bufferPlano);
//      System.out.println("TEXTO CIFRADO");
//      mostrarBytes(bufferCifrado);
//      System.out.println("\n-------------------------------");
//      
//      
//      // PASO 3b: Poner cifrador en modo DESCIFRADO 
//      cifrador.init(Cipher.DECRYPT_MODE, clavePrivada); // Descrifra con la clave privada
//      
//      System.out.println("3b. Descifrar con clave privada");
//      byte[] bufferPlano2 = cifrador.doFinal(bufferCifrado);
//      System.out.println("TEXTO DESCIFRADO");
//      mostrarBytes(bufferPlano2);
//      System.out.println("\n-------------------------------");
//      
//      // PASO 3a: Poner cifrador en modo CIFRADO 
//      cifrador.init(Cipher.ENCRYPT_MODE, clavePrivada);  // Cifra con la clave publica
//
//      System.out.println("4a. Cifrar con clave privada");
//      bufferCifrado = cifrador.doFinal(bufferPlano);
//      System.out.println("TEXTO CIFRADO");
//      mostrarBytes(bufferCifrado);
//      System.out.println("\n-------------------------------");
//      
//      
//      // PASO 3b: Poner cifrador en modo DESCIFRADO 
//      cifrador.init(Cipher.DECRYPT_MODE, clavePublica); // Descrifra con la clave privada
//      
//      System.out.println("4b. Descifrar con clave publica");
//      bufferPlano2 = cifrador.doFinal(bufferCifrado);
//      System.out.println("TEXTO DESCIFRADO");
//      mostrarBytes(bufferPlano2);
//      System.out.println("\n-------------------------------");
   } // Fin main
   
   
   
   public static byte[] leerLinea(java.io.InputStream in) throws IOException {
      byte[] buffer1 = new byte[1000];
      int i =0;
      byte c;
      c = (byte) in.read();
      while((c != '\n') && (i <1000)) {
	 buffer1[i] = c;
	 c = (byte) in.read();
	 i++;
      } 
      
      byte[] buffer2 = new byte[i];
      for(int j=0; j <i; j++) {
	 buffer2[j]=buffer1[j];
      }   
      return(buffer2);
   }

   public static void mostrarBytes(byte [] buffer) {
		System.out.write(buffer, 0, buffer.length);
   } 
	
} // Fin clase


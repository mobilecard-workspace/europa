package com.addcel.utils;

import java.math.BigInteger;
import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Security;
import java.security.spec.RSAPrivateCrtKeySpec;
import java.security.spec.RSAPublicKeySpec;

import javax.crypto.Cipher;

import org.apache.log4j.Logger;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.util.encoders.Base64;
// Necesario para usar el provider Bouncy Castle (BC)
//    Para compilar incluir el fichero JAR en el classpath
// 
public class AddcelCryptoRSA  {
	
	private static Logger logger = Logger.getLogger(AddcelCryptoRSA.class);
	
//	private static final RSAPublicKeySpec pub2048KeySpec = new RSAPublicKeySpec(
//            new BigInteger("bc36306e78cd76b36b9c1efa5b8ffdf950efb0f4cb3003512473b337fa2d9ef25b9f3d99343b5db8e89d388f9cc83d300155f90215b2b7a57e177c9d17c1197bf7b3eb96fb56e41e3fb3dbd54d1f186a0543fdedb2d6c80b6028aedc6f1c9f90bc9cfe3b6e752fc48fb78911a0dfb71d735bd7c323ca319ec427ed5281a6c27dec2bc1da9bc1c2ef912e3c6a2b09267fd85de7a0967fd829370af645ce5dd158bc9c0509eaf5a44a9103a274d2ce6e13eb517a889f16b4611f8e4bca31a71abcd32b2d39461fb50ff0a1aa3ab131c511dd4f873dd60a68774394474a1ac2bce5e86414a950b2bbcddc2cff82cfb5814ec99a7c3434005703d090c1085ee52f4f", 16),
//            new BigInteger("10001", 16));
	
	private static final RSAPublicKeySpec pub2048KeySpec = new RSAPublicKeySpec(
			  new BigInteger("a16f58f3a6e8c5e3ba32e279cb39b6eff3c6bb0e2009dc8f33a587285f66519aa039437feb106f825193936fbe9c96c2a9f7c0bc05782300e9d017b3c73785e77a01a6d517a99db57baec0fe8fe1c21784e980f66d764e02fba3c0471fb6e92cd83f5c23f2f1507d38ec4ee1b092be34eaf0327b1a25a98a6d5c24c78199d80b2432f13f038b04f0c31cce5e36e25daa81c208ec2b6ecdb3090e07eb5406eeecb872abeb57c9fd440144325b3794a5e8b8ca3850c1027251c2b16ee03b34aff9432f5e60d85b89eea9e22c6636771605a89d48450e35eed4f64a1515dcc0b67a519f86d5bb1c5e85725323614615ac2462063bc0da5521183c6f60fa5ac30183", 16),
			  new BigInteger("10001", 16));

    private static final RSAPrivateCrtKeySpec priv2048KeySpec = new RSAPrivateCrtKeySpec(
            new BigInteger("bc36306e78cd76b36b9c1efa5b8ffdf950efb0f4cb3003512473b337fa2d9ef25b9f3d99343b5db8e89d388f9cc83d300155f90215b2b7a57e177c9d17c1197bf7b3eb96fb56e41e3fb3dbd54d1f186a0543fdedb2d6c80b6028aedc6f1c9f90bc9cfe3b6e752fc48fb78911a0dfb71d735bd7c323ca319ec427ed5281a6c27dec2bc1da9bc1c2ef912e3c6a2b09267fd85de7a0967fd829370af645ce5dd158bc9c0509eaf5a44a9103a274d2ce6e13eb517a889f16b4611f8e4bca31a71abcd32b2d39461fb50ff0a1aa3ab131c511dd4f873dd60a68774394474a1ac2bce5e86414a950b2bbcddc2cff82cfb5814ec99a7c3434005703d090c1085ee52f4f", 16),
            new BigInteger("10001", 16),
            new BigInteger("ab88efe71f6e936abefed134f85269f4da913a444113875262d89854c24ce5fa990aaf077c9546bc0a3590a1431134c315d4975811128b51e773dc7d08735398aa8f6e7e72a024d474d6a5dae7a91985373b5aa9533f719efc8ad7a3cc18d84cffba4df3389ed164625fb22b40e5e46c9100f9cabe49bfa3a5bf860a9663b8099c3ec8fc1165dc6184aba4ccf67f42c904066f33e02c7e7aea31fb4f5ccf1a68646b09741099ceaad117949a7f9c2af8abd6dba4bdbda37dd6eb688a0847ffbed9bc3ae876a96599ef7e6d6ff5ac8bcf004440de9b4f6095f6110453b1d748816dee729d8f4d555d0caed6e4f075670fc6371d4535d542e8368cae67e24574f1", 16),
            new BigInteger("fc503b472cef3b10025c886ed2ada30996e3ae7d78080182d8549f357bd6318b111428fd4877b297b2de92792cbf17247051cd007aac3a74923292511d8873258ee9adc1e0cd516c17cdc451264b4e46614d7b489eddca85a01ab5465883fcc49df0f09c7fe6af763a27ae359269c630895745c7c807577991b2cb650d503b2b", 16),
            new BigInteger("bef63007c085835bc9392018bff2b0d4f468e715e75ff8b33531d671f2838c1fdccdf1abeaecd55da9fdd8e64b8e0c0a03ff39cbaed55cd77f70d4b321f2335c5aa87ec4e33e65fdd8e971a1f8912ace8d860a67157ece0b304a3c0fa5b47e23f46e63bf745a9209659b4028aa775699545e51b4dde65c15a71f909703dcfa6d", 16),
            new BigInteger("581c14774883ca564edccd7015f638e712603400f0b7aaff6c5830b8d6a3f7ed1e6bb1dcb13fcb881a188b81f15451da91b69f2639b087414a048afff4963fcfafd67ead23b9cf6335f4cf479f5dd0d9d704ddb68073e7533e93d515d9b74dbfc37fee2658abd03e3314c44b44f3d717fffe100a321a0e3c0fdb261805cc2cbd", 16),
            new BigInteger("1bcb73943d4671a73373b82342150dfe01111fc98562e7ad15ea0a1f8dbfedee59523481e470d2618eb39f156e1bdfceb96435c25ad13d71346074e519e4c9eb005b14108e4eefc37206107f15d1f35c1b274e6e50a7a34fe79876838487eff045cca7821f6a6b6528bfdf27cc20109fc2b2f3cb929ccbfc6ed5de52ccc22645", 16),
            new BigInteger("df14758d07bde52965f7ed198e5ef4da913cb985942e7089aa7db5aeb55de6b5449cb4d5933e4a5fea1994f9628b9343e9c3dbcdea8d9fd5beff57f3cb1c6799810aedb695607111f862a2c1a76df6bee77147dc43146bbc4772f7dc2525fd288ad05036a23a504640b89acc8a1c0dcff762e56d880abe19e085b3f1b7fe28fc", 16)
            );
    
    private static KeyFactory fact;
    private static PrivateKey priv2048Key;
    private static PublicKey pub2048Key;
    private static Cipher cifrador;
    
    static{
    	try{
    		String Name= "BC";
            if (Security.getProvider(Name) == null){
                logger.info("The Provider \"BC\" is not installed." );
                logger.info("The Provider now is installing." );
                Security.addProvider(new BouncyCastleProvider());
                logger.info("The Provider now is installed." );
            }else{
                logger.info("The Provider \"BC\" is installed." );
            }
    		
    		fact = KeyFactory.getInstance("RSA", "BC");
    		priv2048Key = fact.generatePrivate(priv2048KeySpec);
    	    pub2048Key = fact.generatePublic(pub2048KeySpec);	
    	    cifrador = Cipher.getInstance("RSA", "BC");
    	}catch(Exception e){
    		logger.error("Errr al generar las instancias de las llaves: " , e);
    	}
    }
    
    
    public static String encryptPublic(String json) {
    	try{
    		logger.info("Cifrar con Clave Publica");
    		cifrador.init(Cipher.ENCRYPT_MODE, pub2048Key);  // Cifra con la clave publica
    	    byte[] bufferCifrado = cifrador.doFinal(json.getBytes());
    	    json = new String( Base64.encode(bufferCifrado));
    	    logger.debug("TEXTO CIFRADO: " + json);
    	}catch(Exception e){
    		logger.error("Errr en el cifrado publico - encryptPublic: " , e);
    		json = null;
    	}
    	return json;
    }
	
    public static String encryptPrivate(String json) {
    	try{
    		logger.info("Cifrar con Clave Privada");
    		cifrador.init(Cipher.ENCRYPT_MODE, priv2048Key);  // Cifra con la clave publica
    	    byte[] bufferCifrado = cifrador.doFinal(json.getBytes());
    	    json = new String( Base64.encode(bufferCifrado));
    	    logger.debug("TEXTO CIFRADO: " + json);
    	}catch(Exception e){
    		logger.error("Errr en el cifrado publico - encryptPublic: " , e);
    		json = null;
    	}
    	return json;
    }
    
    
    public static String decryptPublic(String json) {
    	try{
    		logger.info("Descifrar con Clave Publica");
    		cifrador.init(Cipher.DECRYPT_MODE, pub2048Key);  // Cifra con la clave publica
    	    byte[] bufferCifrado = cifrador.doFinal(Base64.decode(json.getBytes()));
    	    json = new String(bufferCifrado);
    	    logger.debug("TEXTO DESCIFRADO: " + json);
    	}catch(Exception e){
    		logger.error("Errr en el cifrado publico - encryptPublic: " , e);
    		json = null;
    	}
    	return json;
    }
    
    public static String decryptPrivate(String json) {
    	try{
    		logger.info("Descifrar con Clave Privada");
    		cifrador.init(Cipher.DECRYPT_MODE, priv2048Key);  // Cifra con la clave publica
    	    byte[] bufferCifrado = cifrador.doFinal(Base64.decode(json.getBytes()));
    	    json = new String(bufferCifrado);
    	    logger.debug("TEXTO DESCIFRADO: " + json);
    	}catch(Exception e){
    		logger.error("Errr en el cifrado publico - encryptPublic: " , e);
    		json = null;
    	}
    	return json;
    }
   
}
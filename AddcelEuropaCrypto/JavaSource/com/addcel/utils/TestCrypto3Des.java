package com.addcel.utils;

import java.security.MessageDigest;

import org.bouncycastle.util.encoders.Base64;

import crypto.FunctionByte;

public class TestCrypto3Des {
	public static void main(String[] args) throws Exception {
//        EncriptionSource esource = new EncriptionSource();
//        String key = "DC101AB52CF894CEE52F61731643B94F";
//        String cad = "{\"idComercio\":1,\"idParametro\":\"12345\",\"idConcepto\":1,\"concepto\":\"Descripcion del productoo servicio\",\"valor\":101.25,\"moneda\":\"COP\",\"securecode\":\"a1b2c3d4\",\"certificado\":\"978d5b9875b698db837c09a8b3520184\",\"urlcomercio\":\"https://comercio.com/respuesta\"}";
//        byte[] keyb = toBytes(key);
//        System.out.println("Cadena Ingreso: " + cad);
//        String enc = new BASE64Encoder().encode((esource.Encrypt(keyb, cad)));
//        System.out.println("Encripta: " + enc);
//        String des = esource.Decrypt(keyb, new BASE64Decoder().decodeBuffer(enc));
//        System.out.println("Desencripta: " + des);
//		
//		generaCertificado();
		
//		String key = "DC101AB52CF894CEE52F61731643B94FABCD12A1B2C3D4E5";
		String key = "C0C852F35C2001EBE2D4DF04655FED61";
		String keysen = "12345678";
		//String cadena=test.getJson();
//		String cadena = "11wkIbRSEeyARh/XosJdO96Jw39Fe27Nj31Kg14a41J9voBIwoEj2B21gXv3R6Zka/V4OApFWA0FSswnbayXljy9wLK4tfuelRMddQBM9AkhL4OWzHIwPowSvWVN+MeTFd+kwqPQUsFlssNsIJfrXx47NxFdX0C1UWDLd6tgOqvDlkshph1sYI3s8b39xwXQcpKPEM+fpQnyvOpRweyHoS34Htzois82ukTzfyabJhtxjv4GXjmduCvBwUc6WoaUmbwsL0ttAWZTM9RCLGNSHwkjCVN79OtDgyejrQ2lq/7zNBDw1mbXPNnGdL7lNZaBr+501gOeUzZMOwZpQruhMvmk6tZxLbj2X5Tmm5YnN8G8sveaZA6FblGKlIgA7oBl1fRkUgGVYWFeQcThDZotUihqNSsP8NRGALjByW1niSn+LhhD4bIHKnONWkf1FCXAyEphGy2OxZEpU2EShmlnb5vQRpZd2E5XM89S0wips8ECRgJx2/eSG4XmpXvR7vhh/jxxeQnpehrGZ5+IPAuXzrL8LcSo6sJNV4V0Ayk7hE9FXml6uQIDh9ThJYaZ3szrg9BBTyRIaK4Pqt5+/HwxGyds+gYUajTS0EriAIRybn0Xf845BhNO/quiRl7ZrK9nt99IrQueMLq4pElpYJexoSe3t9OYHM/1clXiayAoF1pAEc0/uUKW9IcPBraCtKnV6AzwtMqZ3de02";
		String cadena = " {\"idComercio\":1,\"idProducto\":\"1001\",\"idConcepto\":\"REF_PRUEBA_IDCONCEPTO\",\"concepto\":\"Compra de voletos de Avion, asietos A1, A2, A3, cadena de pruebas PROD\",\"valor\":22222.00,\"impuesto\":16,\"valorImpuesto\":3065,\"moneda\":\"COP\",\"secureCode\":\"1Xassd3ewv\",\"certificado\":\"giUOmsmYXVo4Anuh1171TQ==\",\"urlComercio\":\"https://comercio.com/respuesta\"}";
//		String cadena = "qrexeli4tuZx1+ospSxDyA==";
//		log.debug(new StringBuffer("CADENA: ").append( cadena ));
		cadena = AddcelCrypto.encryptTripleDes(key, cadena);
//		
//		System.out.println("CAD lenght : " + cadena.getBytes().length);
//		
		cadena = AddcelCrypto.decryptTripleDes(key, cadena);
//		String cadena ="1";
//		253
//		getBase64Cadena(getCadenaBase64(cadena));
//		generaCertificado();
		
		// moneda, idAplicacion, idConcepto, secureCode, valor
		getCertificadoMD5("COP", "1", "REF_PRUEBA_IDCONCEPTO", "1Xassd3ewv", "22222.00");
    }
	
	public static String getCadenaBase64(String cadena){
		String cad = "";
		String respuesta = null;
		try
		{
			System.out.println("cadena: " + cadena);
			
			byte[] cadenaBytes= cadena.getBytes();		
			
			for(int i = 0; i < cadenaBytes.length; i ++){
				cad += cadenaBytes[i];
			}
			System.out.println("cadena bytes:" + cad);
			
//			byte[] md5c = DigestUtils.md5(certificado);
			byte[] base64 = Base64.encode(cadenaBytes);
			System.out.println("cadena base64: " + base64);
			
			respuesta = new String(base64);
			
			System.out.println("final: " + respuesta);
		}catch(Exception e){
			e.printStackTrace();
		}
		return respuesta;
	};
	
	public static String getBase64Cadena(String cadena){
		String cad = "";
		String respuesta = null;
		try
		{
			System.out.println("cadena: " + cadena);
			respuesta = new String (Base64.decode(cadena));
			
			System.out.println("final: " + respuesta);
		}catch(Exception e){
			e.printStackTrace();
		}
		return respuesta;
	};
	
	/**
     * Metodo para convertir un String en un arreglo de 8 bytes
     * @param s String a convertir
     * @return Un arreglo de 8 bytes
     */
    private static byte[] toBytes(String s){
        byte Resultado[] = null;
        try {
            Resultado = new byte[s.length() / 2];
            for (int i = 0; i < Resultado.length; i++){
                Resultado[i] = (byte)Integer.parseInt(s.substring(i * 2, i * 2 + 2), 16);
            }
            return Resultado;
        }catch(Exception e){
            return Resultado;
        }
    }
    
    
    public static void generaCertificado(){
		//String llaveBanco="��6��	pP;E5\n�";
		String cPago = "COP^1^1^ed81fadcaf^123.20";
		String cPagoBytes = "";

		try
		{
			System.out.println("Cadena: " + cPago);
			
			byte[] cadenaPago= cPago.getBytes();	
			
			for(int i = 0; i < cadenaPago.length; i++){
				cPagoBytes += cadenaPago[i];
			}
			
			System.out.println("Cadena Bytes: " + cPagoBytes);
			/* Crear función resumen */
			MessageDigest messageDigest = MessageDigest.getInstance("MD5"); // Usa MD5
			messageDigest.update(cadenaPago);
			cPagoBytes = "";
			byte[] md5c = messageDigest.digest();
			for(int i = 0; i < md5c.length; i++){
				cPagoBytes += md5c[i];
			}
			
			System.out.println("Cadena md5 Bytes: " + cPagoBytes);
			
			byte[] base64 = Base64.encode(md5c);
			cPagoBytes = "";
			for(int i = 0; i < base64.length; i++){
				cPagoBytes += base64[i];
			}
			System.out.println("Cadena base64 Bytes: " + cPagoBytes);
			
			System.out.println("Certificado: " + new String(base64));
			
		}catch(Exception e){
			e.printStackTrace();
		}
	}
    
    public static String getCertificadoMD5(String moneda, String idAplicacion, String idConceto,
    		String secureCode, String valor){
		StringBuffer cadena = new StringBuffer();
		String respuesta = null;
		String cad = "";
		try
		{
			cadena.append(moneda).append("^");
			cadena.append(idAplicacion).append("^");
			cadena.append(idConceto).append("^");
			cadena.append(secureCode).append("^");
			cadena.append(valor);
			System.out.println("cadena: " + cadena);
			
			byte[] cadenaBytes= cadena.toString().getBytes();		
			for(int i = 0; i < cadenaBytes.length; i ++){
				cad += cadenaBytes[i] + " ";
			}
			System.out.println("Cadena bytes:" + cad);
			cad = "";
			MessageDigest messageDigest = MessageDigest.getInstance("MD5"); // Usa MD5
			messageDigest.update(cadenaBytes);
			cad = "";
			byte[] md5c = messageDigest.digest();
			for(int i = 0; i < md5c.length; i ++){
				cad += md5c[i] + " ";
			}
			System.out.println("Cadena bytes MD5:" + cad);
			cad = "";
			byte[] base64 = Base64.encode(md5c);
			for(int i = 0; i < base64.length; i ++){
				cad += base64[i] + " ";
			}
			System.out.println("Cadena bytes Base64:" + cad);
			
			respuesta = new String(base64);
			System.out.println("Certificado: " + respuesta);
		}catch(Exception e){
			e.printStackTrace();
		}
		return respuesta;
	}
}

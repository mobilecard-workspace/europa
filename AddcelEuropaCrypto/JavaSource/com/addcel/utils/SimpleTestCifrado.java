package com.addcel.utils;

import java.math.BigInteger;
import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.RSAPrivateCrtKeySpec;
import java.security.spec.RSAPublicKeySpec;

import javax.crypto.Cipher;

import org.bouncycastle.util.encoders.Base64;

public class SimpleTestCifrado{
	
	static RSAPublicKeySpec pub2048KeySpec = new RSAPublicKeySpec(
            new BigInteger("cba8fb7c93a0ea0a3975f9c3575fb60131c27593717e049d79cbb6e953ad5e426830b352bd4adaf322b90f2b8faa5591a319fd6d422ba2fd17d241ab4111ce4526e4e33c3d889411f67976d0580802f6cac5603989ff82bc0641dd1954d312e8316f83293bc5d77cdf7e33566b725447270cda401a6ca9fbd1a5147c9758f636a767de37676a4cf47fbd7e54bf5b27d731abda27481deaa74f6a9c53da2df2e29000dd0ce585f8a9317060a941433acd360f79f40638298b4ef36a0c2ac76a04ac872328c7bf1582fb3fce548c37a9d654cfdd2ed66c017cf426e9dd20d5918cda41712528bbc20245a9c4fd54359d63df351bfcad086cda1908d1b445cbddb9", 16),
            new BigInteger("10001", 16));

    static RSAPrivateCrtKeySpec priv2048KeySpec = new RSAPrivateCrtKeySpec(
            new BigInteger("cba8fb7c93a0ea0a3975f9c3575fb60131c27593717e049d79cbb6e953ad5e426830b352bd4adaf322b90f2b8faa5591a319fd6d422ba2fd17d241ab4111ce4526e4e33c3d889411f67976d0580802f6cac5603989ff82bc0641dd1954d312e8316f83293bc5d77cdf7e33566b725447270cda401a6ca9fbd1a5147c9758f636a767de37676a4cf47fbd7e54bf5b27d731abda27481deaa74f6a9c53da2df2e29000dd0ce585f8a9317060a941433acd360f79f40638298b4ef36a0c2ac76a04ac872328c7bf1582fb3fce548c37a9d654cfdd2ed66c017cf426e9dd20d5918cda41712528bbc20245a9c4fd54359d63df351bfcad086cda1908d1b445cbddb9", 16),
            new BigInteger("10001", 16),
            new BigInteger("bc81f40d92440e7d5a4465e3b69b23c7253cbe41b47a1561819bdaf94b779c58d141caaa1a0610759e3d36da76470e9f181df5624d2e40d60d57936b25768abb9d4d9d3b64256810dc57ff6ec71cdb497dd485c59c83018a722c42afb0c112ae131c0986800a16f44195f984d33d66ded9395f46986ede43f4ffdcdf99e341391ad2b486c75605a45904d8c16360ec03776c730420b6d0c8320df3fe7da7617ef2a4a67b3c08060236ccd68f24478c6aacdeb6be89753225424061881ddd3a573f176fc75a4a802a6cbff6649b2121ca9fd8b6dd36e133572b05bb301d2d4850562b55674e8bbdc10ad960a607272c0793d1e68de991a2db0af40f2da941aa85", 16),
            new BigInteger("faf2f95a9db3d5c524aaaf86b9f218e51bab4d30242655570cc11cea6aee7e1587c6522c723ca49429ebd511c57dd6c1600ad31c7c8a961bbd30b969d435eaa4b9c6540e8289ad9e5d909e38497bd10377c7915346b285f9f8f21448db43fd8a9e34d6887cbc641dc0129e1ca850e324612730a2a0da6a629cfafad217a7734f", 16),
            new BigInteger("cfc2597f6226884ac12ac7850ed7569c6b3534333516af10a22e4f83e583bbdbdb664351e96a9df48e517f1cddf35424c7be7026d2453786e5965cb7d47014d378301ef0d61cf70205c1a464039c091ed1d38fef00293086a9f90105b674e77b2db2a3dc23a7f960a161fad3631eb419abfd2c0f7b156666ddc8d04c08757c77", 16),
            new BigInteger("8ada020a9793b7c67b414e19b7ad330521c828cf6851258b22ed7f385dae54694954fc86c736b2c1a06872de806dde73e4d658753c9bd9eec341c075c40b892757fb5aeb2463558378b5da35bec319a557933edfdb10507d3db7846dc394dd26e3decc8d7ca728a6256ea7b9a79009fe491b4c17cff4ca44822384c0d681ba95", 16),
            new BigInteger("41cee6f87187910e54c78f317775004b45ae319db2c83fa5be297b28654fe7f6893588476eef459f4c61462947a8a6d38bada7eab41a16af95139ec4dff6acb21b54eac8109a56277ed94a901f909256ffd1cabac2cb4942ae9e6013862adb93e9f2b6f2a91cb0e249dc364e350c69b1e407aaa830ada3a51bc55b2d6a54849b", 16),
            new BigInteger("24d8ae129c801acd60e4dd266ebefedbc759affd9c16d7e796eeef8975b073dd627a2e25488f3b60a590cd454355a7380bc0bef58ae4c81e3c684d3190e2511b431c40ffded6b827e2442a5bda1f2a3da1287862161f03d3b7411903cbc04ee054be59462f5a17a72ae04e1c55fc92a964a36f659664befae780173bf0f2ebb3", 16)
            );

	
    public static void main (String[] args)
    {
    	try{
    		String cadena = "1234567890123456";
    		
	    	KeyFactory          fact = null;
	    	fact = KeyFactory.getInstance("RSA", "BC");
	    	PrivateKey  priv2048Key = fact.generatePrivate(priv2048KeySpec);
	        PublicKey   pub2048Key = fact.generatePublic(pub2048KeySpec);
        
	       
	     // PASO 2: Crear cifrador RSA
	      Cipher cifrador = Cipher.getInstance("RSA", "BC"); // Hace uso del provider BC
//	      PASO 3a: Poner cifrador en modo CIFRADO 
	      cifrador.init(Cipher.ENCRYPT_MODE, pub2048Key);  // Cifra con la clave publica
	
	      System.out.println("3a. Cifrar con clave publica");
//	      byte[] bufferCifrado = cifrador.doFinal(cadena.getBytes());
	      
	      
//	      byte[] base64String = Base64.decode(cadena);
//	      byte[] plainBytes = new String(base64String).getBytes("UTF-8");
	      byte[] bufferCifrado = cifrador.doFinal(cadena.getBytes());
	      
	      String cad = new String( Base64.encode(bufferCifrado));
	      System.out.println("TEXTO CIFRADO");
	      System.out.println(new String(bufferCifrado));
	      System.out.println(bufferCifrado);
	      System.out.println("lengt: " + bufferCifrado.length);
	      System.out.println("\n-------------------------------");
	      
	      
	      bufferCifrado = Base64.decode(cad.getBytes());
//	      byte[] bufferCifrado = cifrador.doFinal(plainBytes);
	      
	      
	      System.out.println("Nueva Cadena: \n" + new String(bufferCifrado));
	      System.out.println(bufferCifrado);
	      System.out.println("lengt: " + bufferCifrado.length);
	      
	      // PASO 3b: Poner cifrador en modo DESCIFRADO 
	      cifrador.init(Cipher.DECRYPT_MODE, priv2048Key); // Descrifra con la clave privada
	      
	      System.out.println("3b. Descifrar con clave privada");
	      byte[] bufferPlano2 = cifrador.doFinal(bufferCifrado);
	      System.out.println("TEXTO DESCIFRADO");
	      System.out.println(new String(bufferPlano2));
	      System.out.println("\n-------------------------------");
	      
	      // PASO 3a: Poner cifrador en modo CIFRADO 
	      cifrador.init(Cipher.ENCRYPT_MODE, priv2048Key);  // Cifra con la clave publica
	
	      System.out.println("4a. Cifrar con clave privada");
	      bufferCifrado = cifrador.doFinal(cadena.getBytes());
	      System.out.println("TEXTO CIFRADO");
	      System.out.println(new String(bufferCifrado));
	      System.out.println("\n-------------------------------");
	      
	      
	      // PASO 3b: Poner cifrador en modo DESCIFRADO 
	      cifrador.init(Cipher.DECRYPT_MODE, pub2048Key); // Descrifra con la clave privada
	      
	      System.out.println("4b. Descifrar con clave publica");
	      bufferPlano2 = cifrador.doFinal(bufferCifrado);
	      System.out.println("TEXTO DESCIFRADO");
	      System.out.println(new String(bufferPlano2));
	      System.out.println("\n-------------------------------");
    	}catch(Exception e){
    		e.printStackTrace();
    	}
        
    }
    
    public static void mostrarBytes(byte [] buffer) {
		System.out.write(buffer, 0, buffer.length);
   } 
}
package com.addcel.utils;

import org.apache.log4j.Logger;

public class TestCryptoRSA {
	private static Logger log = Logger.getLogger(TestCryptoRSA.class);
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		TestCryptoRSA test = new TestCryptoRSA();
		String key = "1234567890ABCDEF0123456789ABCDEF";
		String keysen = "12345678";
		//String cadena=test.getJson();
//		String cadena = "1234567890123456";
//		String cadena = "  { \"id_bitacora\":0, \"tenActualizacion\":\"14.41\", \"wkey\":\"000000000000000\", \"tenencia\":\"1872.69\", \"derRecargo\":\"36.32\", \"vigencia\":\"2013-11-30\", \"subsidio\":false, \"recargos\":\"201.81\", \"password\":\"12345678\", \"linea_captura\":\"84105XX100ABCDVK96UW\", \"actualizacion\":\"17.57\", \"ejercicio\":\"2013\", \"software\":\"4.3\", \"statusPago\":0, \"tipoServicio\":\"PARTICULAR\", \"valor_fact\":\"345000\", \"token\":\"08+P2trH2znqcTEgPlqmo74u453qa10cf21goSte298kD8pehbVLw=\", \"placa\":\"100ABC\", \"condRecargos\":\"0.00\", \"derActualizacion\":\"3.16\", \"cve_vehi\":\"0040720\", \"tenSubsidio\":\"0.00\", \"cvv2\":\"123\", \"banco\":\"986\", \"vigenciaD\":\"Nov 30, 2013 12:00:00 AM\", \"error\":\"\", \"condonacion\":false, \"imei\":\"000000000000000\", \"cy\":\"0.0\", \"cx\":\"0.0\", \"modelo_procom\":\"sdk\", \"id_producto\":2, \"tenRecargo\":\"165.49\", \"fech_factura\":\"2005-04-22\", \"tenCondRecargo\":\"0.00\", \"totalPago\":\"2503.00\", \"derechos\":\"411.00\", \"id_usuario\":\"1372462440000\", \"tipo\":\"unknown\", \"modelo\":\"2005\", \"totalDerecho\":\"450.48\", \"interes\":false }";
		String cadena = "10/22";
//		log.debug(new StringBuffer("CADENA: ").append( cadena ));
//		cadena = AddcelCryptoRSA.encryptPrivate(cadena);
		cadena = AddcelCryptoRSA.encryptPublic(cadena);
		
//		log.debug(new StringBuffer("FINAL: ").append( cadena ));
//		cadena = cadena.replace("+", " ");
		
		cadena = AddcelCryptoRSA.decryptPrivate(cadena);
//		cadena = AddcelCryptoRSA.decryptPublic(cadena);
	}

	private String json;
	public String getJson() {
		return json;
	}
	public void setJson(String json) {
		this.json = json;
	}
	public TestCryptoRSA(){
		getObjeto();
	}
	public TestCryptoRSA(String json){
		this.json = json;
	}
	
	private void getObjeto(){
		this.json = " "+
		" { " +
		    " \"placa\":\"100ABC\", " +
		    " \"condonacion\":false, " +
		    " \"interes\":false, " +
		    " \"subsidio\":false, " +
		    " \"error\":\"\", " +
		    " \"ejercicio\":2013, " +
		    " \"modelo\":2005, " +
		    " \"total\":2379, " +
		    " \"lineacaptura\":\"84105XX100ABC7RK52JV\", " +
		    " \"vigencia\":\"Jul 31, 2013 1:00:00 AM\" , " +
		    " \"tipoServicio\":\"PARTICULAR\", " +
		    " \"cve_vehi\":\"0040720\", " +
		    " \"fech_factura\":\"2005-04-22\", " +
		    " \"tenencia\":1872.69, " +
		    " \"derecho\":411, " +
		    " \"tenActualizacion\":0, " +
		    " \"tenRecargo\":78.09, " +
		    " \"derActualizacion\":0, " +
		    " \"derRecargo\":17.13, " +
		    " \"totalDerecho\":428.13, " +
		    " \"derechos\":411, " +
		    " \"actualizacion\":0, " +
		    " \"recargos\":95.22, " +
		    " \"tenSubsidio\":0, " +
		    " \"tenencia2\":\"1872.69\", " +
		    " \"derechos2\":\"411.00\", " +
		    " \"tenSubsidio2\":\"0.00\", " +
		    " \"total2\":\"2379.00\", " +
		    " \"vigencia2\":1375250400000 " +
		" } ";
		
		
	}
}

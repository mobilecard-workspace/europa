package com.addcel.europa.bridge.utils;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.addcel.europa.bridge.model.vo.CorreoVO;
import com.addcel.europa.bridge.ws.servicios.vo.PaymentDataResponseVO;
import com.addcel.europa.bridge.ws.servicios.vo.PaymentDataVO;
import com.addcel.utils.Utilerias;

public class AddCelGenericMail {
	private static final Logger logger = LoggerFactory.getLogger(AddCelGenericMail.class);
	
	
	private static final String HTML_SUBJECT_PAGO_PRODUCTOS = "Payment Accuse - ";
	private static final String HTML_BODY_PAGO_PRODUCTOS = 
			"<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\"> <html xmlns=\"http://www.w3.org/1999/xhtml\" xml:lang=\"es\" lang=\"es\"> <head> <title>Welcome MobileCard</title> <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />  <style type=\"text/css\"> * { padding: 0; margin: 0; }  body { font-size: 62.5%; background-color: rgb(255, 255, 255); font-family: verdana, arial, sans-serif; } a:link{ color: #000000; font-weight: bold;} a:active{ color: #000000; font-weight: bold;} a:visited{ color: #000000; /*text-decoration: none;*/ font-weight: bold;} a:hover{ color: #FF0000; /*text-decoration: underline;*/ font-weight: bold; } .page-container { width: 700px; margin: 0px auto; margin-top: 10px; margin-bottom: 10px; border: solid 1px rgb(150, 150, 150); font-size: 1.0em; }  /* HEADER */ .header { width: 700px; height: 82px; font-family: \"trebuchet ms\", arial, sans-serif; overflow: visible !important /*Firefox*/; overflow: hidden /*IE6*/; }  .main-content { display: inline; /*Fix IE floating margin bug*/; float: left; width: 640px; margin: 0 0 0 30px; overflow: visible !important /*Firefox*/; overflow: hidden /*IE6*/; margin-bottom: 10px !important /*Non-IE6*/; margin-bottom: 5px /*IE6*/; }  /* MAIN CONTENT */ .main-content h1.pagetitle { margin: 0 0 0.4em 0; padding: 0 0 2px 0; font-family: Verdana, Geneva, sans-serif; color: #000000; font-weight: bold; font-size: 180%; }  .main-content p { margin: 0 0 1.0em 0; line-height: 1.5em; font-size: 120%; text-align: justify; color: #000000; }  .main-content table { clear: both; width: 100%; margin: 0 0 0 0; table-layout: fixed; border-collapse: collapse; empty-cells: show; background-color: rgb(233, 232, 244); text-align: justify; }  .main-content table td { height: 2.5em; padding: 2px 5px 2px 5px; border-left: solid 2px rgb(255, 255, 255); border-right: solid 2px rgb(255, 255, 255); border-top: solid 2px rgb(255, 255, 255); border-bottom: solid 2px rgb(255, 255, 255); background-color: rgb(225, 225, 225); text-align: center; font-weight: normal; color: #000000; font-size: 11.5px; }  /*  FOOTER SECTION  */ .footer { clear: both; width: 700px; height: 7em; padding: 1.1em 0 0; background: rgb(225, 225, 225); font-size: 1.0em; overflow: visible !important /*Firefox*/; overflow: hidden /*IE6*/; }  .footer p { line-height: 1.3em; text-align: center; color: rgb(125, 125, 125); font-weight: bold; font-size: 104%; } </style> </head> <body> <div class=\"page-container\"> <div class=\"header\"> <img src=\"cid:identifierCID00\" alt=\"\" /> </div> <br/>  <div class=\"main-content\">  <h1 class=\"pagetitle\">Estimated: <#NOMBRE#></h1>  <p> Many thanks for <strong>your payment</strong> with us, <strong> MobileCard</strong>. </p> <p>Your payment has been made successfully.</p> <h3>&nbsp;</h3> <p> <font size=+1><strong>Confirmation of payment.</strong></font> </p> <h3>&nbsp;</h3> <table align=\"center\" cellpadding=\"2\"> <tbody> <tr> <td>Date:</td> <td><#DATE#></td> </tr> <tr> <td>Product Description:</td> <td><#PRODUCTO#></td> </tr> <tr> <td>Total:</td> <td>$ <#MONTO#> <#MONEDA#></td> </tr> <tr> <td>Card Number:</td> <td><#CARD#></td> </tr> <tr> <td>Bank Authorization:</td> <td><#AUTORIZACION#></td> </tr> <tr> <td>Reference:</td> <td><#REFERENCIA#></td> </tr> <tr> <td>Parameter:</td> <td><#PARAMETER#></td> </tr> </tbody> </table> <p>&nbsp;</p> <p align=center> <font size=-1>¡Thank you for using MobileCARD!</font> </p>  <p> For questions and / or clarifications you can contact us through The phone +52 (55) 55403124 or write to <a href=mailto:soporte@addcel.com>soporte@addcel.com</a> </p> <p> You can also visit our website: <a href=\"http://www.mobilecard.mx\" title=\"Visistar pagina MobilieCard\">www.mobilecard.mx</a> </p>  <h3>&nbsp;</h3>  </div> <div class=\"footer\"> <p>NOTE. This email is informative, it's not necessary respond to it.</p> <br /> <p>MobileCard - ABC Capital © 2014 | All rights reserved.</p> <p>Powered by Addcel.</p> </div> </div> </body> </html>";
	
	private static final String from = "info@addcel.com";
	
	public static CorreoVO generatedMailPagoProductos(PaymentDataVO paymentData, PaymentDataResponseVO paymentDataResponse){
		logger.info("Genera objeto pago de productos para envio de mail: " + paymentData.getMail());
		
		CorreoVO correo = new CorreoVO();
		String body = null;
		String[] cid = null;
				
		try{
			body = HTML_BODY_PAGO_PRODUCTOS.toString();
			body = body.replaceFirst("<#NOMBRE#>", paymentData.getFullName());
			body = body.replaceFirst("<#PRODUCTO#>", paymentData.getDescProduct());
			body = body.replaceFirst("<#MONTO#>", Utilerias.formatoImporteMon(paymentData.getTotal()));
			body = body.replaceFirst("<#MONEDA#>", paymentData.getMoneda().equals("484")?"MXN":
												 paymentData.getMoneda().equals("840")?"USD":
											     paymentData.getMoneda().equals("540")?"EUR":"");
			body = body.replaceFirst("<#DATE#>", paymentDataResponse.getDate());
			body = body.replaceFirst("<#AUTORIZACION#>", paymentDataResponse.getAuthorizationNumber());
			body = body.replaceFirst("<#REFERENCIA#>", String.valueOf(paymentDataResponse.getReference()));
			body = body.replaceFirst("<#CARD#>", "XXXX XXXX XXXX " + paymentData.getMascara());
			body = body.replaceFirst("<#PARAMETER#>", paymentData.getParametro());
			
			
			cid = new String[]{
					"/usr/java/resources/images/Addcel/MobileCard_header.PNG"
					};
			
			String[] to = {paymentData.getMail()};
			
	//		correo.setAttachments(attachments);
			correo.setBcc(new String[]{});
			correo.setCc(new String[]{});
			correo.setCid(cid);
			correo.setBody(body);
			correo.setFrom(from);
			correo.setSubject(HTML_SUBJECT_PAGO_PRODUCTOS + paymentData.getDescProduct());
			correo.setTo(to);
			
		}catch(Exception e){
			correo = null;
			logger.error("Ocurrio un error al enviar el email ", e);
		}
		return correo;
	}
	

	public static void sendMail(String data) {
		String line = null;
		StringBuilder sb = new StringBuilder();
		try {
			logger.info("Iniciando proceso de envio email. ");
			//logger.debug("data = " + data);

			URL url = new URL(ConstantesEuropa.URL_MAIL_SENDER);
			logger.info("Conectando con " + ConstantesEuropa.URL_MAIL_SENDER);
			HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

			urlConnection.setDoOutput(true);
			urlConnection.setRequestProperty("Content-Type", "application/json");
			urlConnection.setRequestProperty("Accept", "application/json");
			urlConnection.setRequestMethod("POST");

			OutputStreamWriter writter = new OutputStreamWriter(urlConnection.getOutputStream());
			writter.write(data);
			writter.flush();

			logger.info("Datos enviados, esperando respuesta");

			BufferedReader reader = new BufferedReader(new InputStreamReader(
					urlConnection.getInputStream()));

			while ((line = reader.readLine()) != null) {
				sb.append(line);
			}

			logger.info("Respuesta del servidor: " + sb);
		} catch (Exception ex) {
			logger.error("Error en: sendMail, al enviar el email: {}", ex);
		}
	}
}

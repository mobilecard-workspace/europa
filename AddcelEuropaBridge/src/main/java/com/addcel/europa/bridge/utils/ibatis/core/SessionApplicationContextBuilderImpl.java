package com.addcel.europa.bridge.utils.ibatis.core;

import org.apache.log4j.Logger;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class SessionApplicationContextBuilderImpl {
	private static Logger log = Logger.getLogger(SessionApplicationContextBuilderImpl.class);

	private static final String resource = "com/addcel/europa/bridge/model/ibatis/spring-context.xml";
	private static ClassPathXmlApplicationContext ctxt;

	public static ClassPathXmlApplicationContext getApplicationContexInstance() {

		if (ctxt == null) {
			try {
				ctxt = new ClassPathXmlApplicationContext(resource);
			} catch (Exception e) {
				log.error("Error al tratar de crear la instancia SqlSessionFActoryImpl.", e);
			}
		}

		return ctxt;
	}
	
	public static ClassPathXmlApplicationContext reloadApplicationContex() {
		try {
			ctxt = new ClassPathXmlApplicationContext(resource);
		} catch (Exception e) {
			log.error("Error al tratar de crear la instancia SqlSessionFActoryImpl.", e);
		}

		return ctxt;
	}

}

package com.addcel.europa.bridge.ws.servicios;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.addcel.europa.bridge.services.EuropaService;
import com.addcel.europa.bridge.utils.UtilsService;

public class PaymentServices {
	private static final Logger logger = LoggerFactory.getLogger(PaymentServices.class);

	public String sendPaymentTransaction(String transactionProcess){
		EuropaService europaService = null;
		try{
			logger.info("Dentro del servicio sendPaymentTransaction...");
			europaService = new EuropaService();
			transactionProcess = europaService.paymentProcesses(transactionProcess);
		}catch(Exception e){
			transactionProcess = UtilsService.getError(100, e);
		}
		
		return transactionProcess;
	}
	
	public String getToken(String getTokenProcess){
		EuropaService europaService = null;
		try{
			logger.info("Dentro del servicio getToken...");
			europaService = new EuropaService();
			getTokenProcess = europaService.generaToken(getTokenProcess);
		}catch(Exception e){
			getTokenProcess = UtilsService.getError(100, e);
		}
		
		return getTokenProcess;
	}

}

package com.addcel.europa.bridge.model.dao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.addcel.europa.bridge.model.vo.pagos.TBitacoraProsaVO;
import com.addcel.europa.bridge.model.vo.pagos.TBitacoraVO;
import com.addcel.europa.bridge.ws.servicios.vo.PaymentDataResponseVO;
import com.addcel.europa.bridge.ws.servicios.vo.PaymentDataVO;

public class PaymentDAO extends SqlMapClientDaoSupport{
		
	private static final Logger log = LoggerFactory.getLogger(PaymentDAO.class);
	
	public String getFechaActual( ){
		String resp = null;
		try{
			resp = (String) getSqlMapClientTemplate().queryForObject("getFechaActual");
		}catch(Exception e){
			resp = "Occurrio un error en la BD: " + e.getMessage();
			log.error("Ocurrio un error en la BD al generar Token. ", e);
		}
		return resp;
	}
	
	public int diferenciaFechaSegundos(String token ){
		int resp = 1000;
		try{
			resp = (Integer) getSqlMapClientTemplate().queryForObject("diferenciaFechaSegundos", token);
		}catch(Exception e){
			//resp = "Occurrio un error en la BD: " + e.getMessage();
			log.error("Ocurrio un error en la BD al generar Token. {}", e.getMessage());
		}
		return resp;
	}
	
	public int insertEuropaDetalle(PaymentDataVO bitacora) {
		int resp = 0;
		try{
			getSqlMapClientTemplate().insert("insertEuropaDetalle", bitacora);
		}catch(Exception e){
			resp = 200;
			log.error("Ocurrio un error en la BD al generar Token. ", e);
		}
		
		return resp;
	}
	
	public int updateEuropaDetalle(PaymentDataResponseVO bitacora) {
		return (Integer )getSqlMapClientTemplate().update("updateEuropaDetalle", bitacora);
	}
	
	public int insertTBitacora(TBitacoraVO bitacora) {
		Integer idBitacora = 0; 
		
		log.info("Datos de la Bitacora: " + bitacora);
		idBitacora = (Integer )getSqlMapClientTemplate().insert("insertTBitacora", bitacora);
		log.info("IdBitacora: " + idBitacora);
		
		return idBitacora;
	}
	
	public int updateTBitacora(TBitacoraVO bitacora) {
		Integer idBitacora = 0; 
		
		log.info("Datos de la Bitacora: " + bitacora);
		idBitacora = (Integer )getSqlMapClientTemplate().update("updateTBitacora", bitacora);

		return idBitacora;
	}
	
	public int insertTBitacoraProsa(TBitacoraProsaVO bitacora) {
		Integer idBitacora = 0; 
		
		log.info("Datos de la Bitacora Prosa: " + bitacora);
		idBitacora = (Integer )getSqlMapClientTemplate().insert("insertTBitacoraProsa", bitacora);
		log.info("IdBitacoraProsa: " + idBitacora);
		
		return idBitacora;
	}
	
	public int updateTBitacoraProsa(TBitacoraProsaVO bitacora) {
		Integer idBitacora = 0; 
		
		log.info("Datos de la Bitacora Prosa: " + bitacora);
		idBitacora = (Integer )getSqlMapClientTemplate().update("updateTBitacoraProsa", bitacora);

		return idBitacora;
	}
	
}

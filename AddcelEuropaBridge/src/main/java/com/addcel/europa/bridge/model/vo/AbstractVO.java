package com.addcel.europa.bridge.model.vo;

public class AbstractVO {
   
	private int idError;
	private String msgError;
	
	public AbstractVO() {}
	
	public AbstractVO(String msgError) {	
		this.msgError = msgError;
	}
	
	public AbstractVO(int idError, String msgError) {	
		this.idError = idError;
		this.msgError = msgError;
	}

	public int getIdError() {
		return idError;
	}

	public void setIdError(int idError) {
		this.idError = idError;
	}

	public String getMsgError() {
		return msgError;
	}

	public void setMsgError(String msgError) {
		this.msgError = msgError;
	}

}
package com.addcel.europa.bridge.ws.servicios.vo;

import com.addcel.europa.bridge.model.vo.AbstractVO;

public class PaymentDataResponseVO extends AbstractVO{
	
	private int reference;
	private String authorizationNumber;
	private String date;
	
	private int status; 
	
	public PaymentDataResponseVO(){
		
	}
	
	public PaymentDataResponseVO(String msgError){
		super(msgError);
	}
	
	public PaymentDataResponseVO(int idError, String msgError){
		super(idError, msgError);
	}
	
	public int getReference() {
		return reference;
	}
	public void setReference(int reference) {
		this.reference = reference;
	}
	public String getAuthorizationNumber() {
		return authorizationNumber;
	}
	public void setAuthorizationNumber(String authorizationNumber) {
		this.authorizationNumber = authorizationNumber;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}
		
}

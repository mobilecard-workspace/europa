package com.addcel.europa.bridge.services;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.axis.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.addcel.europa.bridge.model.dao.CoreDAO;
import com.addcel.europa.bridge.model.dao.PaymentDAO;
import com.addcel.europa.bridge.model.vo.pagos.TBitacoraProsaVO;
import com.addcel.europa.bridge.model.vo.pagos.TBitacoraVO;
import com.addcel.europa.bridge.model.vo.prosa.ProsaPagoVO;
import com.addcel.europa.bridge.utils.ConstantesEuropa;
import com.addcel.europa.bridge.utils.UtilsValidate;
import com.addcel.europa.bridge.utils.ibatis.service.AbstractService;
import com.addcel.europa.bridge.ws.servicios.vo.PaymentDataResponseVO;
import com.addcel.europa.bridge.ws.servicios.vo.PaymentDataVO;
import com.addcel.utils.AddcelCrypto;
import com.google.gson.Gson;

@Service
public class EuropaService extends AbstractService{
	private static final Logger logger = LoggerFactory.getLogger(EuropaService.class);
	private Gson gson = new Gson();
		
	private static final String KEY_TOKEN = "BSbweBcq6OJbFLAs6MSFlyk7IkijA7dM";
	
	private static final String patronCom = "yyyy-MM-dd";
	private static final SimpleDateFormat formatoCom = new SimpleDateFormat(patronCom);

	public String generaToken(String json){	
		PaymentDAO dao = null;
		try{
			dao = (PaymentDAO) getBean("PaymentDAO");
				
//			if(!USER_TOKEN.equals(tokenVO.getUsuario()) || !PASS_TOKEN.equals(tokenVO.getPassword())){
//				json="{\"idError\":2,\"mensajeError\":\"No tiene permisos para consumir este webservice.\"}";
//			}else{
				json="{\"token\":\""+AddcelCrypto.encrypt(KEY_TOKEN, dao.getFechaActual())+"\",\"idError\":0,\"mensajeError\":\"\"}";
//			}
		}catch(Exception e){
			json="{\"idError\":-1,\"mensajeError\":\"An error occurred while generating the Token.\"}";
		}
//		json = AddcelCrypto.encryptHard(json);
				
		return json;
	}

	
	public String paymentProcesses(String json){		
		PaymentDataResponseVO resp = null;
		PaymentDataVO paymentData = null;
		int idBitcora = 0;
		String respValidacion = null;
		try {
			paymentData = gson.fromJson(json, PaymentDataVO.class);
			
//			paymentData.setCardNumber(paymentData.getCardNumber() != null? AddcelCryptoRSA.decryptPrivate(paymentData.getCardNumber()): null);
//			paymentData.setExpirationDate(paymentData.getExpirationDate() != null? AddcelCryptoRSA.decryptPrivate(paymentData.getExpirationDate()): null);
//			paymentData.setCvv2(paymentData.getCvv2() != null? AddcelCryptoRSA.decryptPrivate(paymentData.getCvv2()): null);
//			paymentData.setTotal(paymentData.getTotal() != null? AddcelCryptoRSA.decryptPrivate(paymentData.getTotal()): null);
			
			respValidacion = validateFieldsPayment(paymentData);
			
			if(respValidacion == null ){
				logger.debug("Inicia proceso de compra.");
				idBitcora = insertaBitacoras(paymentData);
				if(idBitcora > 0){
						resp = enviarPagoVisa(paymentData);
				}else{
					resp = new PaymentDataResponseVO(200, "An error occurred when saving bitacoras.");
				}
			}else{
				resp = new PaymentDataResponseVO(210, respValidacion);
			}
		}catch(Exception e){
			resp = new PaymentDataResponseVO(201, "A general error occurred during payment: " + e.getMessage());
			logger.error("An error occurred general durante el pago: {}", e);
		}finally{
			json = gson.toJson(resp);
		}
		return json;
	}
	
	private PaymentDataResponseVO enviarPagoVisa(PaymentDataVO paymentData){
		PaymentDataResponseVO respuesta = null;
		ProsaPagoVO prosaPagoVO = null;
		String concepto = null;
		try{
			StringBuffer data = new StringBuffer() 
					.append( "card="      ).append( URLEncoder.encode(paymentData.getCardNumber(), "UTF-8") )
					.append( "&vigencia=" ).append( URLEncoder.encode(paymentData.getExpirationDate(), "UTF-8") )
					.append( "&nombre="   ).append( URLEncoder.encode(paymentData.getFullName(), "UTF-8") )
					.append( "&cvv2="     ).append( URLEncoder.encode(paymentData.getCvv2(), "UTF-8") )
					.append( "&monto="    ).append( URLEncoder.encode(paymentData.getTotal() + "", "UTF-8") )
					.append( "&moneda="    ).append( URLEncoder.encode(paymentData.getMoneda(), "UTF-8") )
					.append( "&afiliacion=" ).append( URLEncoder.encode(paymentData.getAfiliacion(), "UTF-8") );
					
			logger.info("Envío de datos EUROPA VISA: {}", data);
			
			URL url = new URL(ConstantesEuropa.URL_AUTH_PROSA);
			URLConnection urlConnection = url.openConnection();
			
			urlConnection.setDoOutput(true);
			
			OutputStreamWriter wr = new OutputStreamWriter(urlConnection.getOutputStream());
			wr.write(data.toString());
			wr.flush();
			logger.info("Datos enviados, esperando respuesta de PROSA");
			
			BufferedReader rd = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
			String line = null;
			StringBuilder sb = new StringBuilder();
			
			while ((line = rd.readLine()) != null) {
				sb.append(line);
			}
			
			wr.close();
			rd.close();
			
			logger.info("Respuesta de PROSA: " + sb);
			
			prosaPagoVO = gson.fromJson(sb.toString(), ProsaPagoVO.class);
			
			if(prosaPagoVO != null){
				respuesta = new PaymentDataResponseVO();
				respuesta.setReference(paymentData.getIdBitacora());
				respuesta.setAuthorizationNumber(prosaPagoVO.getAutorizacion());
				respuesta.setIdError(Integer.parseInt(
						prosaPagoVO.getClaveRechazo()!= null && !"".equals(prosaPagoVO.getClaveRechazo())? prosaPagoVO.getClaveRechazo(): "0"));
				respuesta.setMsgError(prosaPagoVO.getDescripcionRechazo());
				respuesta.setDate(formatoCom.format(new Date()));
				
				
				if(prosaPagoVO.isAuthorized()){
					concepto = "EXITO PAGO EUROPA VISA AUTORIZADA";
					respuesta.setStatus(1);
					respuesta.setMsgError("The payment was done successfully.");
					
					//AddCelGenericMail.sendMail( gson.toJson(AddCelGenericMail.generatedMailPagoProductos(paymentData, respuesta)));
					
				}else if(prosaPagoVO.isRejected()){
					concepto = "ERROR PAGO EUROPA VISA RECHAZADA";
					respuesta.setStatus(0);
					respuesta.setIdError(Integer.parseInt(
						prosaPagoVO.getClaveRechazo()!= null && !"".equals(prosaPagoVO.getClaveRechazo())? prosaPagoVO.getClaveRechazo(): "1"));
					respuesta.setMsgError(ConstantesEuropa.CODE_ERROR_BANK.get(respuesta.getIdError()));
					
				}else if(prosaPagoVO.isProsaError()){
					concepto = "ERROR PAGO EUROPA VISA ERROR";
					respuesta.setStatus(0);
					respuesta.setIdError(Integer.parseInt(
							prosaPagoVO.getClaveRechazo()!= null && !"".equals(prosaPagoVO.getClaveRechazo())? prosaPagoVO.getClaveRechazo(): "1"));
					respuesta.setMsgError(prosaPagoVO.getMsg());
				}
				
				updateBitacoras(respuesta, concepto);
			}
		}catch(Exception e){
			respuesta = new PaymentDataResponseVO();
			logger.error("An error occurred during the call to the bank: {}", e);
			respuesta.setStatus(200);
			respuesta.setMsgError("An error occurred during the call to the bank.");
			updateBitacoras(respuesta, "ERROR PAGO EUROPA VISA GENERAL");
		}
		return respuesta;
	}
	
	private int insertaBitacoras(PaymentDataVO paymentData){		
		TBitacoraVO tbitacora = null;
		PaymentDAO dao = null;
		int idBitadora = 0;
		try{
			
			dao = (PaymentDAO) getBean("PaymentDAO");
			tbitacora = new TBitacoraVO(
					0, paymentData.getIdCompany(), String.valueOf(paymentData.getTotal()), "PAGO EUROPA PRODUCTO", 
					"Sistema Europa", "PAGO EUROPA PRODUCTO: " + paymentData.getDescProduct(),
					paymentData.getCardNumber(), "WEB", "", 
					"", "");
			
			paymentData.setMascara(paymentData.getCardNumber().substring(paymentData.getCardNumber().length() -4 ));
			tbitacora.setTarjetaCompra(AddcelCrypto.encryptTarjeta(paymentData.getCardNumber()));
			
			logger.debug("Insertar en t_bitacora...");
			idBitadora = dao.insertTBitacora(tbitacora);
			paymentData.setIdBitacora(idBitadora);
			tbitacora.setIdBitacora(idBitadora);
			
			logger.debug("idBitacora: {}" , tbitacora.getIdBitacora());
			
			TBitacoraProsaVO tbitacoraProsa = new TBitacoraProsaVO( tbitacora,"0.00","0.00");
			
			logger.debug("Insertar en t_bitacoraProsa...");
			dao.insertTBitacoraProsa(tbitacoraProsa);
			
			logger.debug("Insertar en EUROPA_detalle...");
			dao.insertEuropaDetalle(paymentData);
		
		}catch(Exception e){
			tbitacora.setIdBitacora(0);
			logger.error("Error general al insertar en bitacoras: {}", e);
		}
		
		return tbitacora.getIdBitacora();
	}
	
	private void updateBitacoras(PaymentDataResponseVO paymentDataResp, String concepto){
		try{
			PaymentDAO dao = (PaymentDAO) getBean("PaymentDAO");
			
			TBitacoraVO tbitacora = new TBitacoraVO();
			tbitacora.setIdBitacora(paymentDataResp.getReference());
			tbitacora.setBitStatus(paymentDataResp.getStatus());
			tbitacora.setBitCodigoError(paymentDataResp.getIdError());
			tbitacora.setBitConcepto(concepto);
			tbitacora.setBitNoAutorizacion(paymentDataResp.getAuthorizationNumber());
			
			logger.debug("Actualizando t_bitacora...");
			dao.updateTBitacora(tbitacora);
			
			logger.debug("Actualizando GPO_RICHARD_detalle...");
			dao.updateEuropaDetalle(paymentDataResp);
		}catch(Exception e){
			logger.error("Error general al actualizar en bitacoras: {}", e);
		}
	}
	
	private String validateFieldsPayment(PaymentDataVO paymentData){
		String respuesta = null;
		PaymentDAO dao = null;
		CoreDAO daoCore = null;
		String cadena = null;
		String[] afilMon = null;
		try{
			respuesta = UtilsValidate.validateFields(paymentData.getToken(),"token", new String[]{UtilsValidate.REQUIRED});
			
			if(StringUtils.isEmpty(respuesta)){
				try{
					dao = (PaymentDAO) getBean("PaymentDAO");
					paymentData.setToken(AddcelCrypto.decrypt(KEY_TOKEN, paymentData.getToken()));
					logger.debug("token ==> {}", paymentData.getToken());
				}catch(Exception e){
					respuesta = "The Transaction is invalid.";
					logger.error("The Transaction is invalid: {}", e.getMessage());
				}
				
				if(StringUtils.isEmpty(respuesta) && dao.diferenciaFechaSegundos(paymentData.getToken()) > 60){
//				if(false){
					respuesta = "The Transaction is invalid.";
					logger.error("The Transaction is invalid.");
				}else {
					respuesta  = UtilsValidate.validateFields(paymentData.getIdCompany() + "","idCompany", new String[]{UtilsValidate.REQUIRED,UtilsValidate.MAXLENGTH + "3"});
					respuesta += UtilsValidate.validateFields(paymentData.getDescProduct(),"descProduct", new String[]{UtilsValidate.REQUIRED,UtilsValidate.MAXLENGTH + "100"});
					respuesta += UtilsValidate.validateFields(paymentData.getFullName(),"fullName", new String[]{UtilsValidate.REQUIRED,UtilsValidate.MINLENGTH + "3",UtilsValidate.MAXLENGTH + "50"});
					respuesta += UtilsValidate.validateFields(paymentData.getMail(),"mail", new String[]{UtilsValidate.EMAIL,UtilsValidate.MAXLENGTH + "50"});
					respuesta += UtilsValidate.validateFields(paymentData.getCardNumber(),"cardNumber", new String[]{UtilsValidate.REQUIRED, UtilsValidate.DIGITS, UtilsValidate.MAXLENGTH + "16"});
					respuesta += UtilsValidate.validateFields(paymentData.getExpirationDate(),"expirationDate", new String[]{UtilsValidate.REQUIRED, UtilsValidate.MINLENGTH + "5", UtilsValidate.MAXLENGTH + "5"});
					respuesta += UtilsValidate.validateFields(paymentData.getExpirationDate(),"expirationDate", new String[]{UtilsValidate.DATE + "mm/yy"});
					respuesta += UtilsValidate.validateFields(paymentData.getCvv2(),"cvv2", new String[]{UtilsValidate.REQUIRED, UtilsValidate.DIGITS, UtilsValidate.MINLENGTH + "3",UtilsValidate.MAXLENGTH + "3"});
					respuesta += UtilsValidate.validateFields(paymentData.getTotal() + "","total", new String[]{UtilsValidate.REQUIRED,UtilsValidate.MAXLENGTH + "10"});
					respuesta += UtilsValidate.validateFields(paymentData.getParametro(),"parametro", new String[]{UtilsValidate.REQUIRED, UtilsValidate.MAXLENGTH + "20"});
				}
			}
			
			if(StringUtils.isEmpty(respuesta)){
				respuesta = null;
				daoCore = (CoreDAO) getBean("CoreDAO");
				cadena = daoCore.getTProveedorAfiliacion(paymentData.getIdCompany());
				if(cadena != null){
					afilMon = cadena.split(",");
					paymentData.setMoneda(afilMon[0]);
					paymentData.setAfiliacion(afilMon[1]);
					
				}else{
					respuesta = "There is no set money and affiliation.";
					logger.error("There is no set money and affiliation.");
				}
			}
		}catch(Exception e){
			logger.error("An error occurred with the validation of the fields: {}" , e);
			respuesta = "An error occurred with the validation of the fields.";
		}
		
		return respuesta;
	}
	
}

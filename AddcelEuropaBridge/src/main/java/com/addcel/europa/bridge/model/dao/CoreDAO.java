package com.addcel.europa.bridge.model.dao;

import org.apache.log4j.Logger;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

public class CoreDAO extends SqlMapClientDaoSupport{
		
	private static final Logger log = Logger.getLogger(CoreDAO.class);

	public String getTProveedorPassword(String token ){
		String resp = null;
		try{
			resp = (String) getSqlMapClientTemplate().queryForObject("getTProveedorPassword", token);
		}catch(Exception e){
			//resp = "Occurrio un error en la BD: " + e.getMessage();
			log.error("Ocurrio un error en la BD al generar Token. ", e);
		}
		return resp;
	}
	
	public String getTProveedorAfiliacion(int idProveedor){
		String resp = null;
		try{
			resp = (String) getSqlMapClientTemplate().queryForObject("getTProveedorAfiliacion", idProveedor);
		}catch(Exception e){
			//resp = "Occurrio un error en la BD: " + e.getMessage();
			log.error("Ocurrio un error en la BD al generar Token. ", e);
		}
		return resp;
	}

	
}

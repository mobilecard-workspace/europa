

$(function() {
	
	$( "#cargando" ).dialog({
		dialogClass: "no-close",
		modal: true,
		resizable: false,
		draggable: false,
		//closeOnEscape: false,
		autoOpen: false
	});
	
	function iniciaCarga(){
		$('#menuForm').submit();
		$("#cargando" ).dialog( "open" );
		$('input[type="submit"]').attr('disabled','disabled');
	}

	$(document).ajaxStart(function() {
		$('input[type="submit"]').attr('disabled','disabled');
		$( "#cargando" ).dialog( "open" );
	});
	
	$(document).ajaxStop(function() {
		$( "#cargando" ).dialog( "close" );
		$('input[type="submit"]').removeAttr('disabled');
	});
	
	$("#menu").click(function(){
		$('#menuForm').attr("action", "welcome");
		return false;
	});
	
	$("#menuTransacciones").click(function(){
		$('#menuForm').attr("action", "busquedaTransaccion");
		iniciaCarga();
		return false;
	});

	$("#menuLogout").click(function(){
		$('#menuForm').attr("action", "logout");
		iniciaCarga();
		return false;
	});
	

	$( "input[type=submit], button" )
		.button()
		.click(function( event ) {
		event.preventDefault();
		return false;
	});
	
	$( "a" )
		.click(function( event ) {
		event.preventDefault();
		return false;
	});
	
});
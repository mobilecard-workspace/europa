$(function() {
	

	/************************************************************
	 * 
	 */
	
	       //define colModel
     var colM = [{ title: "ID Reference", width: 70, dataType: "string", dataIndx: "idBitacora" },
                 { title: "Company", width: 100, dataType: "string", dataIndx: "company" },
                 { title: "Status", width: 55, dataType: "string",  dataIndx: "status", 
                	 render: function (ui) {
                		var rowData = ui.rowData; 
                        if (rowData["status"] == 'Approved') {
                            return "<span style='color:green;'>" + rowData["status"] + "</span>";
                        }
                        else if (rowData["status"] == 'Canceled') {
                        	return "<span style='color:blue;'><strong>" + rowData["status"] + "</strong></span>";
                        }
                        else {
                            return "<span style='color:red;'>" + rowData["status"] + "</span>";
                        }
                     }},
                 { title: "Customer Name", width: 100, dataType: "string",  dataIndx: "fullName" },
                 { title: "Card", width: 50, dataType: "string",  dataIndx: "cardNumber", align:"right"  },
                 { title: "Total", width: 70, dataType: "string",  dataIndx: "total", align:"right"  },
                 { title: "Mon", width: 40, dataType: "string",  dataIndx: "moneda", align:"center"},
                 { title: "Num Autho", width: 70, dataType: "string",  dataIndx: "numAutorizacion", align:"right" },
                 { title: "Date", width: 120, dataType: "string",  dataIndx: "fecha" },
                 { title: "Cod Error", width: 30, dataType: "string",  dataIndx: "claveError" },
                 { title: "Des Error", width: 100, dataType: "string",  dataIndx: "descError" },
                 { title: "Desc Product", width: 200, dataType: "string", dataIndx: "descProduct" },
                 { title: "Parameter", width: 100, dataType: "string", dataIndx: "parametro" }
                 ];
	         
     
     //define dataModel
     var dataModel = {
         location: "remote",
         sorting: "remote",
         paging: "local",
         dataType: "JSON",
         method: "POST",
         curPage: 1,
         rPP: 20,
         sortIndx: 0,
         sortDir: "up",
         rPPOptions: [20, 50, 100],
         filterIndx: "",
         filterValue: "",
         getUrl: function () {
             var sortDir = (this.sortDir == "up") ? "asc" : "desc";
             /*var sort = ['idBitacora', 'company', 'status', 'fullName', 'cardNumber', 'total', 'moneda',
                          'claveError', 'descError', 'descProduct', 'parametro'];*/
             var queryString = "curPage=" + this.curPage + "&recordsPerPage=" + this.rPP + 
                 "&sortBy=" + this.sortIndx + "&dir=" + sortDir
                 + "&tipoBusqueda=" + $("input[name='tipoBusqueda']:checked").val()
                 + "&status=" + $("input[name='status']:checked").val();
             if($("#company").val() != null){
            	 queryString = queryString + "&idCompany=" + $("#company").val();
             }
             /*if (this.filterIndx != null && sort [this.filterIndx] ) {
                 queryString += "&filterBy=" + this.filterIndx + "&filterValue=" + this.filterValue;
             }else{
            	 queryString += "&filterBy=0&filterValue=";
             }*/
             var obj = { url: "/AddcelEuropaWeb/portal/filtroTransaccion", data: queryString };
             return obj;
         },
         getData: function (dataJSON) {
             return { curPage: dataJSON.curPage, totalRecords: dataJSON.totalRecords, data: dataJSON.data };
         }
     };
     
     var obj = { 
    		 dataModel: dataModel,
    	     colModel: colM,
    		 width: 984, 
    		 height: 600, 
    		 title: "Grid Transacctions", 
    		 flexHeight: false, 
    		 flexWidth: false,
    		 topVisible: false, 
    		 bottomVisible: true,
    		 columnBorders: true, 
    		 rowBorders: true, 
    		 oddRowsHighlight: true, 
    		 numberCell: false, 
    		 scrollModel: {horizontal: true}, 
    		 resizable: false, 
    		 draggable: false,
    		 editable: true
    		 //freezeCols: 3
    	};
     
    var $grid = $("#grid_json").pqGrid(obj);
    
    $("#searchButton").click(function(){
    	$grid.pqGrid("refreshDataAndView");
	});
    
    /*$grid.pqGrid("option", $.paramquery.pqGrid.regional["es"]);
    $grid.find(".pq-pager").pqPager("option", $.paramquery.pqPager.regional["es"]);
	*/
	/*************************************************************
	 * Fin Busqueda de Transaccion
	 */
    
});
<!DOCTYPE html>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" 		  prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<title>Start session in Addcel Europa Portal </title>

<!-- Meta Tags -->
<link rel="icon" type="image/x-icon" href="<c:url value="/resources/images/icono_45x45.png"/>">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="Pragma" content="no-cache" >
<meta http-equiv="expires" content="-1" >

<%	
   	response.setHeader("Expires","0");
   	response.setHeader("Pragma","no-cache");
   	response.setDateHeader("Expires",-1);
 	
%>
<!-- CSS -->
<link href="<c:url value="/resources/css/theme.css"/>" rel="stylesheet">
<link href="<c:url value="/resources/css/form.css"/>" rel="stylesheet">
<link href="<c:url value="/resources/css/smoothness/jquery-ui.css"/>" rel="stylesheet">

<STYLE type="text/css">
   .no-close .ui-dialog-titlebar {
   		display: none;
   }
</STYLE>

<script src="<c:url value="/resources/js/sistema/jquery-1.9.0.js"/>"></script>
<script src="<c:url value="/resources/js/sistema/jquery-ui.js"/>"></script>
<script src="<c:url value="/resources/js/sistema/jquery.validate.js"/>"></script>
<script src="<c:url value="/resources/js/sistema/additional-methods.js"/>"></script>
<script src="<c:url value="/resources/js/sistema/messages_es.js"/>"></script>

<script>
$(function() {

	$( "#cargando" ).dialog({
		dialogClass: "no-close",
		modal: true,
		resizable: false,
		draggable: false,
		//closeOnEscape: false,
		autoOpen: false
	});

	$(document).ajaxStart(function() {
		$('input[type="submit"]').attr('disabled','disabled');

		$( "#cargando" ).dialog( "open" );
	});
	
	$(document).ajaxStop(function() {
		$( "#cargando" ).dialog( "close" );
		$('input[type="submit"]').removeAttr('disabled');
	});

	$("#saveForm").click(function(){
		$("#registroForm").submit(); //SUBMIT FORM
	});  


	$("#loginSubmit").click(function(){

		$('#loginForm').submit(function(event) {
			$('input[type="submit"]').attr('disabled','disabled');

			if(!$( this ).valid()){
				$('input[type="submit"]').removeAttr('disabled');
				event.preventDefault(); //STOP default action
				event.stopImmediatePropagation();
			    return false;
			}else{
				$( "#cargando" ).dialog( "open" );
			}

		});

		$("#loginForm").submit(); //SUBMIT FORM
	});  

	$( "input[type=submit], button" )
		.button()
		.click(function( event ) {
		event.preventDefault();
	});

	
	$("#loginForm").validate({
        rules :{
        	login : {
                required  : true,
                maxlength : 15  //para validar campo con maximo 9 caracteres
            },
            password : {
                required : true, 
                maxlength : 16
            }

        }
    });

});
</script>


</head>

<body id="body">
	<div id="cargando" align="center" style="display: none;">
		<p>Sending information...</p>
		<p><img src="<c:url value="/resources/images/loading_1.gif"/>" /></p>
	</div>

	<div id="container" >
		<div class="header">
			<div class="header-top">
				<img src="<c:url value="/resources/images/mobilecard_header.png"/>" alt="Powered by mobilecard" />
			</div>
			<!-- <div class="header-breadcrumbs"></div> -->
		</div>
		
		<form:form id="loginForm" name="loginForm" class="mobilecard topLabel page"
			autocomplete="off" method="post" modelAttribute="usuario"
			action="${pageContext.request.contextPath}/portal/loginFinal">

			<div id="header" class="info">
				<label class="desc" style="font-size: 16px"> Login Addcel Europa </label>
				<label class="desc" style="font-size: 12px"> Please input the following data 
			</div>

			<ul>
				<li id="foliError" class="complex notranslate      " >
					<label class="error" > 
						${mensaje}
					</label>
				</li>
			
				<li id="foli0" class="complex notranslate      ">
					<label class="desc" id="title9" for="Field9"> User 
						<span id="req_9" class="req">*</span>
					</label>
					<div>
						<span class="left zip">
							<form:input id="login" path="login" maxlength="15" />
						</span>
					</div>
				</li>

				<li id="foli1" class="complex notranslate      ">
					<label class="desc" id="title9" for="Field9"> Password
						<span id="req_9" class="req">*</span>
					</label>
					<div>
						<span class="left zip">
							<form:input type="password" id="password" path="password" maxlength="16" />
						</span>
					</div>
				</li>
				
			</ul>
			<div align="center"  style="height: 50px;">
			<input id="loginSubmit" name="loginSubmit" type="submit" value="Login" style="width: 150px"/>
			<!-- <input id="resetSubmit" name="resetSubmit" type="submit" value="Reset Password" style="width: 150px"/> -->
		</div>
		</form:form>
		
		<br><br><br>&nbsp;
		<div class="footer">
			<p>Addcel Europa © 2014 | All rights reserved.</p>
			<p>Powered by Addcel.</p>
			<p class="credits">&nbsp;</p>
		</div>

	</div>
	<!--container-->
	
</body>
</html>

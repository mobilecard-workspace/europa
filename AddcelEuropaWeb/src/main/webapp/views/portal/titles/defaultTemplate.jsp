<!DOCTYPE html>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" 		prefix="c" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" 	prefix="tiles" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<title><tiles:insertAttribute name="title" ignore="true" /></title>
<link rel="icon" type="image/x-icon" href="<c:url value="/resources/images/mobilecard_icono_24x24.png"/>">

<!-- Meta Tags -->
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="Pragma" content="no-cache" >
<meta http-equiv="expires" content="-1" >

<%	
   	response.setHeader("Expires","0");
   	response.setHeader("Pragma","no-cache");
   	response.setDateHeader("Expires",-1);
 	
%>
<!-- CSS -->
<link href="<c:url value="/resources/css/menu.css"/>" rel="stylesheet">
<link href="<c:url value="/resources/css/form.css"/>" rel="stylesheet">
<link href="<c:url value="/resources/css/theme.css"/>" rel="stylesheet">
<link href="<c:url value="/resources/css/smoothness/jquery-ui.css"/>" rel="stylesheet">

<script src="<c:url value="/resources/js/sistema/jquery-1.9.0.js"/>"></script>
<script src="<c:url value="/resources/js/sistema/jquery-ui.js"/>"></script>
<script src="<c:url value="/resources/js/sistema/jquery.validate.js"/>"></script>
<script src="<c:url value="/resources/js/sistema/additional-methods.js"/>"></script>
<script src="<c:url value="/resources/js/sistema/jquery.ui.local.js"/>"></script>
<script src="<c:url value="/resources/js/sistema/messages_es.js"/>"></script>
<script src="<c:url value="/resources/js/addcel.js"/>"></script>

<STYLE type="text/css">
	.no-close .ui-dialog-titlebar {
		display: none;
	}
	.ui-widget {
	    font-size: 1em;
	}
</STYLE>


</head>

<body id="body">
	<div id="cargando" align="center" style="display: none;">
		<p>Sending information...</p>
		<p><img src="<c:url value="/resources/images/loading_1.gif"/>" /></p>
	</div>
	
	<div id="container" >
		
		<tiles:insertAttribute name="header" />
		
		<tiles:insertAttribute name="menu" />
		
		<tiles:insertAttribute name="body" />

		<tiles:insertAttribute name="footer" />

	</div>
</body>
</html>

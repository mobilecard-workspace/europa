<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" 		prefix="c" %>

<!-- Meta Tags -->
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="Pragma" content="no-cache" >
<meta http-equiv="expires" content="-1" >

<%	
   	response.setHeader("Expires","0");
   	response.setHeader("Pragma","no-cache");
   	response.setDateHeader("Expires",-1);
 	
%>
<link href="<c:url value="/resources/css/pqgrid.min.css"/>" rel="stylesheet">
<link href="<c:url value="/resources/css/pqgrid.css"/>" rel="stylesheet">

<script src="<c:url value="/resources/js/sistema/pqgrid.min.js"/>"></script>
<script src="<c:url value="/resources/js/busquedaTransaccion.js"/>"></script>


	<div id="container" >

		<form id="busqueda" name=""busqueda"" class="mobilecard topLabel page"
			autocomplete="on" method="post" 
			action="${pageContext.request.contextPath}/">

			<div id="header" class="info">
				<label class="desc" style="font-size: 16px"> Transaction Search </label>
			</div>
			
			<ul>
				<li class="complex notranslate      " >
					<label id="lblErrorPago" class="error" > 
						${mensaje}
					</label>
				</li>
				<li class="complex notranslate  " >
					<table align="left" cellpadding="2"  >
                        <tbody>
                        	<tr>
                        		<td>
                        			<input type="radio" name="tipoBusqueda" value="1" checked><label>Current Day </label>
                        		</td>
                        		<td>
                        			<input type="radio" name="tipoBusqueda" value="2"><label>Current Week </label>
                        		</td>
                        		<td>
                        			<input type="radio" name="tipoBusqueda" value="3"><label>Current Month </label>
                        		</td>
                        		<td>
                        			<input type="radio" name="tipoBusqueda" value="4"><label>Previous Month </label>
                        		</td>
                        	</tr>
                        	<tr>
                        		<td>
                        			<input type="radio" name="status" value="-1" checked><label>All </label>
                        		</td>
                        		<td>
                        			<input type="radio" name="status" value="1"><label>Approved </label>
                        		</td>
                        		<td>
                        			<input type="radio" name="status" value="0"><label>Rejected </label>
                        		</td>
                        		<td>
                        			<input type="radio" name="status" value="2"><label>Canceled </label>
                        		</td>
                        	</tr>
                        	<c:if test="${companias != null}">
                        		<tr>
                        			<td>
	                        			<label>Company </label>
	                        		</td>
	                        		<td colspan="3">
	                        			<select name="company" id="company"  >
					              			<option value="0" >-- All companies --</option>
		                        			<c:forEach items="${companias}" var="company">
										       <option value="${company.value}">${company.label}</option>
										   </c:forEach>
									   </select>
	                        		</td>
	                        	</tr>
							</c:if>
                        	<tr>
                        		<td colspan="4">
                        			<input id="searchButton" name="searchButton" type="submit" value="Transaction Search" style="width: 150px"/>
                        		</td>
                        	</tr>
                        </tbody>
                	</table>
				</li>
			</ul>
			<div id="grid_json" style="aling: center;"></div>
			<br>
		</form>
		
	</div>
	<!--container-->
	


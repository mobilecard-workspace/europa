package com.addcel.europa.portal.model.mapper;

import java.util.HashMap;
import java.util.List;

import com.addcel.europa.portal.model.vo.PaymentData;
import com.addcel.europa.portal.model.vo.PaymentDataRequest;
import com.addcel.europa.portal.model.vo.UsuarioVO;

public interface PortalConfigMapper {
	
	public List<UsuarioVO> validarUsuario(UsuarioVO usuario);
	
	public List<PaymentData> buscarPagos(PaymentDataRequest paymentData);
	
	public List<HashMap<String, String>> buscarCompanias();
	
}

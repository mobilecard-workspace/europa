package com.addcel.europa.portal.services;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.ModelAndView;

import com.addcel.europa.portal.model.mapper.PortalConfigMapper;
import com.addcel.europa.portal.model.vo.AbstractVO;
import com.addcel.europa.portal.model.vo.LoginVO;
import com.addcel.europa.portal.model.vo.PaymentData;
import com.addcel.europa.portal.model.vo.PaymentDataRequest;
import com.addcel.europa.portal.model.vo.PaymentDataResponse;
import com.addcel.europa.portal.model.vo.UsuarioVO;
import com.addcel.europa.portal.utils.ConstantesEuropaWeb;
import com.addcel.europa.portal.utils.UtilsService;
import com.addcel.utils.AddcelCrypto;

@Service
public class EuropaPortalService {
	private static final Logger logger = LoggerFactory.getLogger(EuropaPortalService.class);
	
	@Autowired
	private PortalConfigMapper portalMapper;
	
	@Autowired
	private UtilsService utilService;
	
	public UsuarioVO login(LoginVO login) {
		UsuarioVO usuario = new UsuarioVO();
		List<UsuarioVO> usuarioOriginal = null;
		try {
			usuario.setLogin(login.getLogin());
			usuarioOriginal = portalMapper.validarUsuario(usuario);
			
			login.setPassword(AddcelCrypto.encryptPassword(login.getPassword()));
			
			if(usuarioOriginal == null || (usuarioOriginal != null && usuarioOriginal.size() == 0)){
				usuario = new UsuarioVO(107, "Login or password incorrect.");
			}else if(!usuarioOriginal.get(0).getPassword().equals(login.getPassword())){
				usuario = new UsuarioVO(107, "Login or password incorrect.");
			}else if(usuarioOriginal.get(0).getStatus() != 1){
				usuario = new UsuarioVO(105, ConstantesEuropaWeb.ERROR_USUARIO_STATUS_INACTIVO);
			}else{
				usuario = usuarioOriginal.get(0);
				usuario.setPassword("");
			}
		}catch(Exception e){
			usuario = new UsuarioVO(2, "An error occurred at login.");
			logger.error("Error en el login: {}", e.getMessage());
			
		}
		return usuario;		
	}	
	
	public String busquedaTransaccion(PaymentDataRequest filtroTransaccion, UsuarioVO usuario){				
		PaymentDataResponse resp = new PaymentDataResponse();
		String json = null;
		try {
			if(validarSession(usuario) == null){
				if(usuario.getRol() != 1){
					filtroTransaccion.setIdCompany(usuario.getIdCompania());
				}
				
				resp.setData(portalMapper.buscarPagos(filtroTransaccion));
				
				if(resp.getData() != null && resp.getData().size() > 0){
					
					resp.setTotalRecords(resp.getData().size() +1);
					resp.setCurPage(1);
				}else{
					resp.setTotalRecords(resp.getData().size() +1);
					resp.setCurPage(1);
					resp.setData(new ArrayList<PaymentData>());
				}
			}else{
				resp.setTotalRecords(0);
				resp.setCurPage(1);
				resp.setData(new ArrayList<PaymentData>());
				resp.getData().add(new PaymentData());
				resp.getData().get(0).setCompany(ConstantesEuropaWeb.ERROR_NO_SESSION);
			}
			
		}catch (Exception pe) {
			logger.error("Ocurrio un error general al buscar tiendas desde el portal: {}", pe.getMessage());
		}finally{
			json = utilService.objectToJson(resp);
		}
		return json;
	}
	
	private AbstractVO validarSession (UsuarioVO usuario){
		AbstractVO resp = null;
		if(usuario == null){
			resp = new AbstractVO(ConstantesEuropaWeb.ID_NO_SESSION, ConstantesEuropaWeb.ERROR_NO_SESSION);
		}
		return resp;
	}
	
	public List<HashMap<String, String>> buscarCompanias (){
		return portalMapper.buscarCompanias();
	}
	
	public ModelAndView validarSessionController (UsuarioVO usuario){
		logger.info("Dentro del servicio: /portal/welcome");
		ModelAndView mav = null;
		try{
			if(usuario == null){
				logger.debug("Se termino la session, portal/login." );
				mav = new ModelAndView("portal/login", "usuario", new LoginVO());
				mav.addObject("mensaje", ConstantesEuropaWeb.ERROR_NO_SESSION);
			}
		}catch(Exception e){
			mav = new ModelAndView("error");
			logger.error("Ocurrio un error general al validar session: {}", e );
		}
		return mav;	
	}
	
}

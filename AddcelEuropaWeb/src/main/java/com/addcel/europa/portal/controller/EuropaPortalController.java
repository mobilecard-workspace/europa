package com.addcel.europa.portal.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;

import com.addcel.europa.portal.model.vo.LoginVO;
import com.addcel.europa.portal.model.vo.PaymentDataRequest;
import com.addcel.europa.portal.model.vo.UsuarioVO;
import com.addcel.europa.portal.services.EuropaPortalService;

/**
 * Handles requests for the application home page.
 */
@Controller
@SessionAttributes({"usuario"})
public class EuropaPortalController {
	
	private static final Logger logger = LoggerFactory.getLogger(EuropaPortalController.class);
	
	@Autowired
	private EuropaPortalService webService;
	
	@RequestMapping(value = "/")
	public ModelAndView login(ModelMap modelo) {	
		logger.debug("Dentro del servicio: /");
		ModelAndView mav = null;
		try{
			logger.debug("Iniciar pantalla, login" );
			mav = new ModelAndView("portal/login", "usuario", new LoginVO());
		}catch(Exception e){
			mav = new ModelAndView("portal/error");
			mav.addObject("mensajeError", "Ocurrio un error general: " + e.getMessage());
		}
		
		return mav;	
	}
	
	@RequestMapping(value = "/portal/login")
	public ModelAndView login(@ModelAttribute("login") LoginVO login, ModelMap modelo) {	
		logger.debug("Dentro del servicio: /login");
		ModelAndView mav = null;
		try{
			logger.debug("Iniciar pantalla, login" );
			mav = new ModelAndView("portal/login", "usuario", new LoginVO());
		}catch(Exception e){
			mav = new ModelAndView("portal/error");
			mav.addObject("mensajeError", "Ocurrio un error general: " + e.getMessage());
		}
		
		return mav;	
	}
	
	@RequestMapping(value = "/portal/loginFinal", method=RequestMethod.POST)
	public ModelAndView loginFinal( @ModelAttribute("login") LoginVO login, ModelMap modelo) {
		logger.debug("Dentro del servicio: /portal/loginFinal");
		ModelAndView mav = null;
		UsuarioVO usuario = null;
			
		try{
			
			logger.debug("Iniciar session, login: {}",login.getLogin() );
			
			usuario = webService.login(login);
			
//			logger.debug("usuario.IdError: {}", usuario.getIdError());
//			logger.debug("usuario.MensajeError: {}", usuario.getMensajeError());
			
			if(usuario.getIdError() == 0){
				mav = new ModelAndView("portal/welcome");
				logger.debug("usuario.nombres: {}", usuario.getNombre());
				logger.debug("usuario.status: {}", usuario.getStatus());
				
				//Se sube a session el objeto Usuario
				modelo.put("usuario", usuario);
				mav.addObject("usuario", usuario);
				
			}else{
				login.setPassword("");
				mav = new ModelAndView("portal/login", "usuario", login);
				mav.addObject("mensaje", usuario.getMensajeError());
			}
			
		}catch(Exception e){
			logger.error("Ocurrio un error general en el servicio /loginFinal: {}", e );
			mav = new ModelAndView("error");
			mav.addObject("mensaje", "Ocurrio un error general: " + e.getMessage());
		}
		return mav;	
	}
	
	@RequestMapping(value = "/portal/logout", method = {RequestMethod.POST, RequestMethod.GET})
	public ModelAndView logout(SessionStatus status, ModelMap modelo) {
		ModelAndView mav = null;
		try{
			status.setComplete();
			modelo.remove("usuario");
			logger.debug("Cerrando session y redireccionando a la pagina de login." );
			mav = new ModelAndView("portal/login", "usuario", new LoginVO());
		}catch(Exception e){
			logger.error("Ocurrio un error general al cerrar session: {}", e );
			mav = new ModelAndView("error");
			mav.addObject("mensaje", "Ocurrio un error general: " + e.getMessage());
		}

		return mav;
	}
	
	@RequestMapping(value = "/portal/busquedaTransaccion", method = {RequestMethod.POST, RequestMethod.GET})
	public ModelAndView busquedaTransaccion( ModelMap modelo ) {
		logger.debug("Dentro del servicio: /portal/busquedaTienda");
		ModelAndView mav = null;
		UsuarioVO usuario = null;
		try{
			usuario = (UsuarioVO) modelo.get("usuario");
			mav = webService.validarSessionController(usuario);
			if(mav == null){
				logger.debug("Inicia pantalla , portal/busquedaTienda." );
				mav = new ModelAndView("portal/busquedaTransaccion");
				
				if(usuario.getRol() == 1){
					mav.addObject("companias", webService.buscarCompanias());
				}
			}
		}catch(Exception e){
			mav = new ModelAndView("error");
			logger.error("Ocurrio un error general en el servicio /portal/busquedaTienda: {}", e );
		}
		return mav;	
	}
	
	@RequestMapping(value = "/portal/filtroTransaccion", method = {RequestMethod.POST, RequestMethod.GET})
	public @ResponseBody String filtroTransaccion(@ModelAttribute("PaymentDataRequest") PaymentDataRequest filtroTransaccion, ModelMap modelo){
		UsuarioVO usuario = null;
		String json = null;
		try{
			usuario = (UsuarioVO) modelo.get("usuario");
			json = webService.busquedaTransaccion(filtroTransaccion, usuario);
			
		}catch(Exception e){
			logger.error("Ocurrio un error general en el servicio /portal/busquedaTienda: {}", e );
		}
		return json;	
	}
	
	
}
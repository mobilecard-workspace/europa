package com.addcel.europa.portal.utils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;

import javax.xml.bind.JAXBContext;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.addcel.europa.portal.model.vo.AbstractVO;
import com.addcel.utils.AddcelCrypto;

@Component
public class UtilsService {
	private static final Logger logger = LoggerFactory.getLogger(UtilsService.class);
	
	@Autowired
	private ObjectMapper mapperJk;	
	
	public <T> String objectToJson(T object){
		String json=null;
		try {
			json = mapperJk.writeValueAsString(object);
//			logger.info("Encoding DEFAULT: {}",json);
			json = new String(json.getBytes(),"UTF-8");
//			logger.info("Encoding ISO-8859-1: {}",json);
		} catch (JsonGenerationException e) {
			logger.error("1.- Error al generar json: {}",e);
		} catch (JsonMappingException e) {
			logger.error("2.- Error al mapperar objeccto: {}",e);
		} catch (IOException e) {
			logger.error("3.- Error al generar json: {}",e);
		}
		return json;
	}

	public <T> Object jsonToObject(String json, Class<T> clase) {		
		Object obj = null;
		try {
			obj = mapperJk.readValue(json, clase);
		} catch (JsonParseException e) {
			logger.error("Error al parsear cadena json 1: {}", e);
		} catch (JsonMappingException e) {
			logger.error("Error al parsear cadena json 2: {}", e);
		} catch (IOException e) {
			logger.error("Error al parsear cadena json 3: {}", e);
		}
		return obj;
	}
	
	private static JAXBContext JC = null;
	
	
	
	public String peticionUrlPostParams(String urlRemota,String parametros){
		String respuesta = null;
		URL url;
		try {
			url = new URL(urlRemota);		
			HttpURLConnection con = (HttpURLConnection) url.openConnection();
			// Se indica que se escribira en la conexi�n
			con.setDoOutput(true);
			con.setRequestMethod("POST");
			// Se escribe los parametros enviados a la url
			OutputStreamWriter wr = new OutputStreamWriter(con.getOutputStream());		
			wr.write(parametros);
			wr.close();			
			if(con.getResponseCode() == HttpURLConnection.HTTP_OK ||
					con.getResponseCode() == HttpURLConnection.HTTP_ACCEPTED){			
				BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream()));
				StringBuilder sb = new StringBuilder();
				String jsonString;
				while ((jsonString = br.readLine()) != null) {
					sb.append(jsonString);
				}
				respuesta=sb.toString();			
			}else{
				respuesta = "ERROR";
			}
			con.disconnect();
			
		} catch (MalformedURLException e) {
			logger.error("ERROR URL malformada: {}",e);
		} catch (IOException e) {
			logger.error("ERROR IO : {}",e);
		}
		return respuesta;
	}
	
	
	public String peticionUrlParams(String urlRemota, String parametros){
		String respuesta = null;
		try {
			logger.debug("URL a consultar: {}", urlRemota);
			logger.debug("PArametros enviados: {}", parametros);
			URL url = new URL(urlRemota);
			URLConnection urlConnection = url.openConnection();
			
			urlConnection.setDoOutput(true);
			
			OutputStreamWriter wr = new OutputStreamWriter(urlConnection.getOutputStream());
			wr.write(parametros);
			wr.flush();
			logger.debug("Datos enviados, esperando respuesta...");
			
			BufferedReader rd = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
			String line = null;
			StringBuilder sb = new StringBuilder();
			
			while ((line = rd.readLine()) != null) {
				sb.append(line);
			}
			
			wr.close();
			rd.close();
			
			logger.debug("Respuesta: {}", sb);
			
		} catch (MalformedURLException e) {
			logger.error("ERROR URL malformada: {}",e);
		} catch (IOException e) {
			logger.error("ERROR IO : {}",e);
		}
		return respuesta;
	}
	
	public String consumeServicio(String urlServicio, String parametros) {
        InputStream is = null;
        OutputStream os = null;
        InputStreamReader isr = null;
        BufferedWriter writer = null;
        BufferedReader br = null;
        HttpURLConnection conn = null;
        URL url = null;
        String inputLine = null;
        StringBuffer sbf = new StringBuffer();
        try {
            url = new URL(urlServicio);
            conn = (HttpURLConnection) url.openConnection();
            if (conn != null) {
                conn.setConnectTimeout(10000);
                conn.setRequestMethod("POST");
                conn.setDoInput(true);
                if (parametros != null) {
                    conn.setDoOutput(true);
                    os = conn.getOutputStream();
                    writer = new BufferedWriter(new OutputStreamWriter(os));
                    writer.write(parametros);
                    writer.close();
                    os.close();
                    conn.connect();
                }
                is = conn.getInputStream();
                isr = new InputStreamReader(is);
                br = new BufferedReader(isr);
                while ((inputLine = br.readLine()) != null) {
                    sbf.append(inputLine);
                }
                logger.debug("Respuesta: {}", sbf);
            }
        } catch (Exception e) {
        	logger.error("Error al leer la url " + url, e);
        } finally {
            if (conn != null) {
                try {
                    conn.disconnect();
                } catch (Exception e) {
                	logger.error("Exception : ", e);
                }
            }
            if (br != null) {
                try {
                    br.close();
                } catch (Exception e) {
                	logger.error("Exception : ", e);
                }
            }

            if (isr != null) {
                try {
                    isr.close();
                } catch (Exception e) {
                	logger.error("Exception : ", e);
                }
            }
            if (is != null) {
                try {
                    is.close();
                } catch (Exception e) {
                	logger.error("Exception : ", e);
                }
            }
        }
        return sbf.toString();
    }
	
	
	public HashMap<String, Object> getRespuestaBusquedas(int idError, String msgExito, String nombre, Object obj){
		HashMap<String, Object> resp = new HashMap<String, Object>();
		resp.put(ConstantesEuropaWeb.NOM_CAMPO_ID_ERROR, idError);
		if(msgExito != null){
			resp.put(ConstantesEuropaWeb.NOM_CAMPO_MENJ_ERROR, msgExito);
		}
		if(nombre != null){
			resp.put(nombre, obj);
		}
		
		
		return resp;
	}
	
	private static final String base = "0123456789abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	
	public String generatePassword(int longitud){
		StringBuffer contrasena = new StringBuffer();
		int numero = 0;
		for(int i = 0; i < longitud; i++){ //1
		    numero = (int)(Math.random()*base.length()); //2
		    contrasena.append(base.substring(numero, numero+1)); //4
		}
		return contrasena.toString();
	}
	
	private static final String MASCARA = "XXXX XXXX XXXX ";
	public String obtenerMascara(String carnet, boolean addX){
		carnet = AddcelCrypto.decryptTarjeta(carnet);
		if(addX){
			carnet = MASCARA + carnet.substring(carnet.length() -4 );
		}
		
		return carnet;
	}
	
}
